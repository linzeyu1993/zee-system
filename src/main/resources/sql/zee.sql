/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : zee

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 18/02/2023 00:53:15
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `pid` bigint(20) NULL DEFAULT NULL COMMENT '上级ID',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `sort` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '排序',
  `creator` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updater` bigint(20) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_pid`(`pid`) USING BTREE,
  INDEX `idx_sort`(`sort`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (1, 2, '技术部', 2, 1, '2022-11-08 17:38:27', 2, '2023-02-04 18:16:24');
INSERT INTO `sys_dept` VALUES (2, 5, '广东分公司', 1, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_dept` VALUES (3, 5, '北京分公司', 0, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_dept` VALUES (4, 3, '市场部', 0, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_dept` VALUES (5, 0, 'zee网络科技', 0, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_dept` VALUES (6, 3, '销售部', 0, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_dept` VALUES (7, 2, '产品部', 1, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_dept` VALUES (8, 1, '总监', 3, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `dict_type_id` bigint(20) NOT NULL COMMENT '字典类型ID',
  `dict_label` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典标签',
  `dict_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典值',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `sort` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '排序',
  `creator` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updater` bigint(20) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_dict_type_value`(`dict_type_id`, `dict_value`) USING BTREE,
  INDEX `idx_sort`(`sort`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典数据' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', '', 0, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_dict_data` VALUES (2, 1, '女', '1', '', 1, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_dict_data` VALUES (3, 1, '保密', '2', '', 2, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_dict_data` VALUES (4, 2, '公告', '0', '', 0, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_dict_data` VALUES (5, 2, '会议', '1', '', 1, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_dict_data` VALUES (6, 2, '其他', '2', '', 2, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典类型',
  `dict_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典名称',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `sort` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '排序',
  `creator` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updater` bigint(20) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典类型' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, 'gender', '性别', '', 0, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_dict_type` VALUES (2, 'notice_type', '站内通知-类型', '', 1, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_dict_type` VALUES (12, 'test3', 'test3', 'test2', 0, 1, '2023-01-17 02:36:31', 1, '2023-01-17 02:36:31');

-- ----------------------------
-- Table structure for sys_log_login
-- ----------------------------
DROP TABLE IF EXISTS `sys_log_login`;
CREATE TABLE `sys_log_login`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `operation` tinyint(3) UNSIGNED NULL DEFAULT NULL COMMENT '操作类型   0：用户登录   1：用户退出',
  `status` tinyint(3) UNSIGNED NOT NULL COMMENT '状态  0：失败    1：成功    2：账号已锁定',
  `user_agent` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户代理',
  `ip` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作IP',
  `creator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `creator` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_status`(`status`) USING BTREE,
  INDEX `idx_create_date`(`create_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '登录日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_log_operation
-- ----------------------------
DROP TABLE IF EXISTS `sys_log_operation`;
CREATE TABLE `sys_log_operation`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `operation` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户操作',
  `request_uri` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求URI',
  `request_method` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求方式',
  `request_params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '请求参数',
  `request_time` int(10) UNSIGNED NOT NULL COMMENT '请求时长(毫秒)',
  `user_agent` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户代理',
  `ip` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作IP',
  `status` tinyint(3) UNSIGNED NOT NULL COMMENT '状态  0：失败   1：成功',
  `creator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `creator` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_create_date`(`create_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 83 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '操作日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `pid` bigint(20) NULL DEFAULT NULL COMMENT '上级ID，一级菜单为0',
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单URL',
  `permissions` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：sys:user:list,sys:user:save)',
  `menu_type` tinyint(3) UNSIGNED NULL DEFAULT NULL COMMENT '类型   0：菜单   1：按钮',
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `creator` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updater` bigint(20) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_pid`(`pid`) USING BTREE,
  INDEX `idx_sort`(`sort`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 42 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 0, '权限管理', NULL, NULL, 0, 'icon-safetycertificate', 0, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_menu` VALUES (2, 1, '角色管理', 'sys/role', NULL, 0, 'icon-team', 2, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_menu` VALUES (3, 1, '用户管理', 'sys/user', NULL, 0, 'icon-user', 0, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_menu` VALUES (4, 3, '新增', NULL, 'sys:user:save,sys:dept:list,sys:role:list', 1, NULL, 1, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_menu` VALUES (5, 3, '修改', NULL, 'sys:user:update,sys:dept:list,sys:role:list', 1, NULL, 2, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_menu` VALUES (6, 3, '删除', NULL, 'sys:user:delete', 1, NULL, 3, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_menu` VALUES (7, 3, '导出', NULL, 'sys:user:export', 1, NULL, 4, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_menu` VALUES (8, 2, '查看', NULL, 'sys:role:page,sys:role:info', 1, NULL, 0, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_menu` VALUES (9, 2, '新增', NULL, 'sys:role:save,sys:menu:select,sys:dept:list', 1, NULL, 1, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_menu` VALUES (10, 2, '修改', NULL, 'sys:role:update,sys:menu:select,sys:dept:list', 1, NULL, 2, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_menu` VALUES (11, 2, '删除', NULL, 'sys:role:delete', 1, NULL, 3, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_menu` VALUES (12, 1, '部门管理', 'sys/dept', NULL, 0, 'icon-apartment', 1, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_menu` VALUES (13, 3, '查看', NULL, 'sys:user:page,sys:user:info', 1, NULL, 0, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_menu` VALUES (14, 0, '系统管理', '', '', 0, 'icon-desktop', 0, 1, '2022-11-22 17:14:18', 1, '2022-11-22 17:14:18');
INSERT INTO `sys_menu` VALUES (15, 14, '菜单管理', 'sys/menu', '', 0, 'icon-unorderedlist', 0, 1, '2022-11-22 17:35:50', 1, '2022-11-22 17:35:50');
INSERT INTO `sys_menu` VALUES (16, 15, '查看', '', 'sys:menu:list,sys:menu:info', 1, '', 0, 1, '2022-11-22 17:42:17', 1, NULL);
INSERT INTO `sys_menu` VALUES (17, 15, '新增', '', 'sys:menu:save', 1, '', 0, 1, '2022-11-22 17:45:23', 1, '2022-11-22 17:45:23');
INSERT INTO `sys_menu` VALUES (18, 15, '修改', '', 'sys:menu:update', 1, '', 0, 1, '2022-11-22 17:47:18', 1, '2022-11-22 17:47:18');
INSERT INTO `sys_menu` VALUES (19, 15, '删除', '', 'sys:menu:delete', 1, '', 0, 1, '2022-11-22 17:48:44', 1, '2022-11-22 17:48:44');
INSERT INTO `sys_menu` VALUES (20, 14, '参数管理', 'sys/params', NULL, 0, 'icon-fileprotect', 0, 1, '2022-11-22 17:35:50', 1, '2022-11-22 17:35:50');
INSERT INTO `sys_menu` VALUES (21, 20, '查看', '', 'sys:params:page,sys:params:list,sys:params:info', 1, '', 0, 1, '2022-11-22 17:42:17', 1, '2023-01-28 01:20:37');
INSERT INTO `sys_menu` VALUES (22, 20, '新增', '', 'sys:params:save', 1, '', 0, 1, '2023-01-24 01:11:31', 1, '2023-01-24 01:11:31');
INSERT INTO `sys_menu` VALUES (23, 20, '修改', '', 'sys:params:update', 1, '', 0, 1, '2023-01-24 01:12:06', 1, '2023-01-24 01:12:06');
INSERT INTO `sys_menu` VALUES (24, 20, '删除', '', 'sys:params:delete', 1, '', 0, 1, '2023-01-24 01:12:21', 1, '2023-01-24 01:12:21');
INSERT INTO `sys_menu` VALUES (25, 12, '查看', NULL, 'sys:dept:list,sys:dept:info', 1, NULL, 0, 1, '2023-01-24 01:32:26', 1, '2023-01-24 01:32:26');
INSERT INTO `sys_menu` VALUES (26, 12, '新增', NULL, 'sys:dept:list,sys:dept:save', 1, NULL, 0, 1, '2023-01-24 01:32:50', 1, '2023-01-24 01:48:45');
INSERT INTO `sys_menu` VALUES (27, 12, '修改', NULL, 'sys:dept:list,sys:dept:update', 1, NULL, 0, 1, '2023-01-24 01:33:12', 1, '2023-01-24 01:48:51');
INSERT INTO `sys_menu` VALUES (28, 12, '删除', NULL, 'sys:dept:delete', 1, NULL, 0, 1, '2023-01-24 01:33:23', 1, '2023-01-24 01:33:31');
INSERT INTO `sys_menu` VALUES (29, 0, '日志管理', '', '', 0, 'icon-container', 0, 1, '2023-01-24 18:28:23', 1, '2023-01-24 18:28:23');
INSERT INTO `sys_menu` VALUES (30, 29, '登录日志', 'sys/log-login', '', 0, 'icon-filedone', 0, 1, '2023-01-24 18:28:54', 1, '2023-01-25 00:08:44');
INSERT INTO `sys_menu` VALUES (31, 29, '操作日志', 'sys/log-operation', '', 0, 'icon-solution', 0, 1, '2023-01-24 18:30:12', 1, '2023-01-25 00:08:22');
INSERT INTO `sys_menu` VALUES (32, 31, '查看', '', 'sys:logOperation:page', 1, '', 0, 1, '2023-01-24 18:35:22', 1, '2023-01-24 18:35:22');
INSERT INTO `sys_menu` VALUES (33, 14, '字典管理', 'sys/dict-type', '', 0, 'icon-golden-fill', 0, 1, '2023-01-25 00:10:11', 1, '2023-01-25 00:10:11');
INSERT INTO `sys_menu` VALUES (34, 30, '查看', '', 'sys:logLogin:page', 1, '', 0, 1, '2023-01-25 01:01:43', 1, '2023-01-25 01:01:43');
INSERT INTO `sys_menu` VALUES (35, 33, '查看', '', 'sys:dict:page,sys:dict:list,sys:dict:info', 1, '', 0, 1, '2023-01-28 01:22:47', 1, '2023-01-28 01:22:47');
INSERT INTO `sys_menu` VALUES (36, 33, '新增', '', 'sys:dict:save', 1, '', 0, 1, '2023-01-28 01:23:42', 1, '2023-01-28 01:23:42');
INSERT INTO `sys_menu` VALUES (37, 33, '修改', '', 'sys:dict:update', 1, '', 0, 1, '2023-01-28 01:24:07', 1, '2023-01-28 01:24:07');
INSERT INTO `sys_menu` VALUES (38, 33, '删除', '', 'sys:dict:delete', 1, '', 0, 1, '2023-01-28 01:24:26', 1, '2023-01-28 01:24:26');
INSERT INTO `sys_menu` VALUES (39, 20, '导出', '', 'sys:params:export', 1, '', 0, 1, '2023-02-16 17:56:08', 1, '2023-02-16 17:56:08');
INSERT INTO `sys_menu` VALUES (40, 30, '导出', '', 'sys:logLogin:export', 1, '', 0, 1, '2023-02-17 17:39:12', 1, '2023-02-17 17:41:35');
INSERT INTO `sys_menu` VALUES (41, 31, '导出', '', 'sys:logOperation:export', 1, '', 0, 1, '2023-02-17 17:39:32', 1, '2023-02-17 17:41:44');

-- ----------------------------
-- Table structure for sys_params
-- ----------------------------
DROP TABLE IF EXISTS `sys_params`;
CREATE TABLE `sys_params`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `param_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数编码',
  `param_value` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数值',
  `param_type` tinyint(3) UNSIGNED NULL DEFAULT 1 COMMENT '类型   0：系统参数   1：非系统参数',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `creator` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updater` bigint(20) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_param_code`(`param_code`) USING BTREE,
  INDEX `idx_create_date`(`create_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '参数管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_params
-- ----------------------------
INSERT INTO `sys_params` VALUES (1, 'CLOUD_STORAGE_CONFIG_KEY', '{\"aliyunDomain\":\"\",\"aliyunPrefix\":\"\",\"aliyunEndPoint\":\"\",\"aliyunAccessKeyId\":\"\",\"aliyunAccessKeySecret\":\"\",\"aliyunBucketName\":\"\",\"qcloudDomain\":\"\",\"qcloudPrefix\":\"\",\"qcloudSecretId\":\"\",\"qcloudSecretKey\":\"\",\"qcloudBucketName\":\"\"}', 0, '云存储配置信息', 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_params` VALUES (2, 'TEST_CONFIG_KEY', '{\"type\":1,\"qiniuDomain\":\"test.com\"}', 1, '测试配置信息', 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `creator` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updater` bigint(20) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_dept_id`(`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, 'test', 'test', 5, 1, '2022-11-29 18:27:32', 1, '2023-02-03 00:23:20');
INSERT INTO `sys_role` VALUES (2, 'test1', '', 5, 1, '2023-02-02 17:50:01', 1, '2023-02-02 17:50:01');
INSERT INTO `sys_role` VALUES (3, 'role2', '', 5, 1, '2023-02-02 18:11:07', 1, '2023-02-02 18:11:07');
INSERT INTO `sys_role` VALUES (4, '1111', '', 1, 2, '2023-02-03 00:24:10', 2, '2023-02-03 00:24:10');

-- ----------------------------
-- Table structure for sys_role_data_scope
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_data_scope`;
CREATE TABLE `sys_role_data_scope`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `creator` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_role_id`(`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色数据权限' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_data_scope
-- ----------------------------
INSERT INTO `sys_role_data_scope` VALUES (3, 2, 5, 1, '2023-02-02 17:50:01', '2023-02-02 17:50:01');
INSERT INTO `sys_role_data_scope` VALUES (4, 2, 3, 1, '2023-02-02 17:50:01', '2023-02-02 17:50:01');
INSERT INTO `sys_role_data_scope` VALUES (5, 2, 4, 1, '2023-02-02 17:50:01', '2023-02-02 17:50:01');
INSERT INTO `sys_role_data_scope` VALUES (6, 2, 6, 1, '2023-02-02 17:50:01', '2023-02-02 17:50:01');
INSERT INTO `sys_role_data_scope` VALUES (7, 2, 2, 1, '2023-02-02 17:50:01', '2023-02-02 17:50:01');
INSERT INTO `sys_role_data_scope` VALUES (8, 2, 7, 1, '2023-02-02 17:50:01', '2023-02-02 17:50:01');
INSERT INTO `sys_role_data_scope` VALUES (9, 2, 1, 1, '2023-02-02 17:50:01', '2023-02-02 17:50:01');
INSERT INTO `sys_role_data_scope` VALUES (10, 2, 8, 1, '2023-02-02 17:50:01', '2023-02-02 17:50:01');
INSERT INTO `sys_role_data_scope` VALUES (11, 3, 3, 1, '2023-02-02 18:11:07', '2023-02-02 18:11:07');
INSERT INTO `sys_role_data_scope` VALUES (12, 3, 4, 1, '2023-02-02 18:11:07', '2023-02-02 18:11:07');
INSERT INTO `sys_role_data_scope` VALUES (13, 3, 6, 1, '2023-02-02 18:11:07', '2023-02-02 18:11:07');
INSERT INTO `sys_role_data_scope` VALUES (14, 1, 1, 1, '2023-02-03 00:23:20', '2023-02-03 00:23:20');
INSERT INTO `sys_role_data_scope` VALUES (15, 1, 8, 1, '2023-02-03 00:23:20', '2023-02-03 00:23:20');
INSERT INTO `sys_role_data_scope` VALUES (16, 4, 8, 2, '2023-02-03 00:24:10', '2023-02-03 00:24:10');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NULL DEFAULT NULL COMMENT '菜单ID',
  `creator` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_role_id`(`role_id`) USING BTREE,
  INDEX `idx_menu_id`(`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 64 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色菜单关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (3, 2, 1, 1, '2023-02-02 17:50:01', '2023-02-02 17:50:01');
INSERT INTO `sys_role_menu` VALUES (4, 2, 3, 1, '2023-02-02 17:50:01', '2023-02-02 17:50:01');
INSERT INTO `sys_role_menu` VALUES (5, 2, 13, 1, '2023-02-02 17:50:01', '2023-02-02 17:50:01');
INSERT INTO `sys_role_menu` VALUES (6, 2, 4, 1, '2023-02-02 17:50:01', '2023-02-02 17:50:01');
INSERT INTO `sys_role_menu` VALUES (7, 2, 5, 1, '2023-02-02 17:50:01', '2023-02-02 17:50:01');
INSERT INTO `sys_role_menu` VALUES (8, 2, 6, 1, '2023-02-02 17:50:01', '2023-02-02 17:50:01');
INSERT INTO `sys_role_menu` VALUES (9, 2, 7, 1, '2023-02-02 17:50:01', '2023-02-02 17:50:01');
INSERT INTO `sys_role_menu` VALUES (10, 2, 12, 1, '2023-02-02 17:50:01', '2023-02-02 17:50:01');
INSERT INTO `sys_role_menu` VALUES (11, 2, 25, 1, '2023-02-02 17:50:01', '2023-02-02 17:50:01');
INSERT INTO `sys_role_menu` VALUES (12, 2, 26, 1, '2023-02-02 17:50:01', '2023-02-02 17:50:01');
INSERT INTO `sys_role_menu` VALUES (13, 2, 27, 1, '2023-02-02 17:50:01', '2023-02-02 17:50:01');
INSERT INTO `sys_role_menu` VALUES (14, 2, 28, 1, '2023-02-02 17:50:01', '2023-02-02 17:50:01');
INSERT INTO `sys_role_menu` VALUES (15, 2, 2, 1, '2023-02-02 17:50:01', '2023-02-02 17:50:01');
INSERT INTO `sys_role_menu` VALUES (16, 2, 8, 1, '2023-02-02 17:50:01', '2023-02-02 17:50:01');
INSERT INTO `sys_role_menu` VALUES (17, 2, 9, 1, '2023-02-02 17:50:01', '2023-02-02 17:50:01');
INSERT INTO `sys_role_menu` VALUES (18, 2, 10, 1, '2023-02-02 17:50:01', '2023-02-02 17:50:01');
INSERT INTO `sys_role_menu` VALUES (19, 2, 11, 1, '2023-02-02 17:50:01', '2023-02-02 17:50:01');
INSERT INTO `sys_role_menu` VALUES (20, 3, 1, 1, '2023-02-02 18:11:07', '2023-02-02 18:11:07');
INSERT INTO `sys_role_menu` VALUES (21, 3, 3, 1, '2023-02-02 18:11:07', '2023-02-02 18:11:07');
INSERT INTO `sys_role_menu` VALUES (22, 3, 13, 1, '2023-02-02 18:11:07', '2023-02-02 18:11:07');
INSERT INTO `sys_role_menu` VALUES (23, 3, 4, 1, '2023-02-02 18:11:07', '2023-02-02 18:11:07');
INSERT INTO `sys_role_menu` VALUES (24, 3, 5, 1, '2023-02-02 18:11:07', '2023-02-02 18:11:07');
INSERT INTO `sys_role_menu` VALUES (25, 3, 6, 1, '2023-02-02 18:11:07', '2023-02-02 18:11:07');
INSERT INTO `sys_role_menu` VALUES (26, 3, 7, 1, '2023-02-02 18:11:07', '2023-02-02 18:11:07');
INSERT INTO `sys_role_menu` VALUES (27, 3, 12, 1, '2023-02-02 18:11:07', '2023-02-02 18:11:07');
INSERT INTO `sys_role_menu` VALUES (28, 3, 25, 1, '2023-02-02 18:11:07', '2023-02-02 18:11:07');
INSERT INTO `sys_role_menu` VALUES (29, 3, 26, 1, '2023-02-02 18:11:07', '2023-02-02 18:11:07');
INSERT INTO `sys_role_menu` VALUES (30, 3, 27, 1, '2023-02-02 18:11:07', '2023-02-02 18:11:07');
INSERT INTO `sys_role_menu` VALUES (31, 3, 28, 1, '2023-02-02 18:11:07', '2023-02-02 18:11:07');
INSERT INTO `sys_role_menu` VALUES (32, 3, 2, 1, '2023-02-02 18:11:07', '2023-02-02 18:11:07');
INSERT INTO `sys_role_menu` VALUES (33, 3, 8, 1, '2023-02-02 18:11:07', '2023-02-02 18:11:07');
INSERT INTO `sys_role_menu` VALUES (34, 3, 9, 1, '2023-02-02 18:11:07', '2023-02-02 18:11:07');
INSERT INTO `sys_role_menu` VALUES (35, 3, 10, 1, '2023-02-02 18:11:07', '2023-02-02 18:11:07');
INSERT INTO `sys_role_menu` VALUES (36, 3, 11, 1, '2023-02-02 18:11:07', '2023-02-02 18:11:07');
INSERT INTO `sys_role_menu` VALUES (37, 1, 1, 1, '2023-02-03 00:23:20', '2023-02-03 00:23:20');
INSERT INTO `sys_role_menu` VALUES (38, 1, 3, 1, '2023-02-03 00:23:20', '2023-02-03 00:23:20');
INSERT INTO `sys_role_menu` VALUES (39, 1, 13, 1, '2023-02-03 00:23:20', '2023-02-03 00:23:20');
INSERT INTO `sys_role_menu` VALUES (40, 1, 4, 1, '2023-02-03 00:23:20', '2023-02-03 00:23:20');
INSERT INTO `sys_role_menu` VALUES (41, 1, 5, 1, '2023-02-03 00:23:20', '2023-02-03 00:23:20');
INSERT INTO `sys_role_menu` VALUES (42, 1, 6, 1, '2023-02-03 00:23:20', '2023-02-03 00:23:20');
INSERT INTO `sys_role_menu` VALUES (43, 1, 7, 1, '2023-02-03 00:23:20', '2023-02-03 00:23:20');
INSERT INTO `sys_role_menu` VALUES (44, 1, 12, 1, '2023-02-03 00:23:20', '2023-02-03 00:23:20');
INSERT INTO `sys_role_menu` VALUES (45, 1, 25, 1, '2023-02-03 00:23:20', '2023-02-03 00:23:20');
INSERT INTO `sys_role_menu` VALUES (46, 1, 26, 1, '2023-02-03 00:23:20', '2023-02-03 00:23:20');
INSERT INTO `sys_role_menu` VALUES (47, 1, 27, 1, '2023-02-03 00:23:20', '2023-02-03 00:23:20');
INSERT INTO `sys_role_menu` VALUES (48, 1, 28, 1, '2023-02-03 00:23:20', '2023-02-03 00:23:20');
INSERT INTO `sys_role_menu` VALUES (49, 1, 2, 1, '2023-02-03 00:23:20', '2023-02-03 00:23:20');
INSERT INTO `sys_role_menu` VALUES (50, 1, 8, 1, '2023-02-03 00:23:20', '2023-02-03 00:23:20');
INSERT INTO `sys_role_menu` VALUES (51, 1, 9, 1, '2023-02-03 00:23:20', '2023-02-03 00:23:20');
INSERT INTO `sys_role_menu` VALUES (52, 1, 10, 1, '2023-02-03 00:23:20', '2023-02-03 00:23:20');
INSERT INTO `sys_role_menu` VALUES (53, 1, 11, 1, '2023-02-03 00:23:20', '2023-02-03 00:23:20');
INSERT INTO `sys_role_menu` VALUES (54, 1, 29, 1, '2023-02-03 00:23:20', '2023-02-03 00:23:20');
INSERT INTO `sys_role_menu` VALUES (55, 1, 30, 1, '2023-02-03 00:23:20', '2023-02-03 00:23:20');
INSERT INTO `sys_role_menu` VALUES (56, 1, 34, 1, '2023-02-03 00:23:20', '2023-02-03 00:23:20');
INSERT INTO `sys_role_menu` VALUES (57, 1, 31, 1, '2023-02-03 00:23:20', '2023-02-03 00:23:20');
INSERT INTO `sys_role_menu` VALUES (58, 1, 32, 1, '2023-02-03 00:23:20', '2023-02-03 00:23:20');
INSERT INTO `sys_role_menu` VALUES (59, 4, 29, 2, '2023-02-03 00:24:10', '2023-02-03 00:24:10');
INSERT INTO `sys_role_menu` VALUES (60, 4, 30, 2, '2023-02-03 00:24:10', '2023-02-03 00:24:10');
INSERT INTO `sys_role_menu` VALUES (61, 4, 34, 2, '2023-02-03 00:24:10', '2023-02-03 00:24:10');
INSERT INTO `sys_role_menu` VALUES (62, 4, 31, 2, '2023-02-03 00:24:10', '2023-02-03 00:24:10');
INSERT INTO `sys_role_menu` VALUES (63, 4, 32, 2, '2023-02-03 00:24:10', '2023-02-03 00:24:10');

-- ----------------------------
-- Table structure for sys_role_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_user`;
CREATE TABLE `sys_role_user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
  `creator` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_role_id`(`role_id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色用户关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_user
-- ----------------------------
INSERT INTO `sys_role_user` VALUES (1, 1, 2, 1, '2022-11-29 18:29:40', '2022-11-29 18:29:40');
INSERT INTO `sys_role_user` VALUES (2, 1, 3, 1, '2023-01-21 17:40:36', '2023-01-21 17:40:36');
INSERT INTO `sys_role_user` VALUES (3, 1, 8, 1, '2023-01-23 16:08:06', '2023-01-23 16:08:06');
INSERT INTO `sys_role_user` VALUES (5, 1, 10, 1, '2023-01-24 00:25:42', '2023-01-24 00:25:42');
INSERT INTO `sys_role_user` VALUES (6, 1, 11, 1, '2023-01-24 00:43:02', '2023-01-24 00:43:02');
INSERT INTO `sys_role_user` VALUES (9, 1, 13, 1, '2023-01-24 01:07:28', '2023-01-24 01:07:28');
INSERT INTO `sys_role_user` VALUES (12, 1, 9, 1, '2023-02-04 18:54:25', '2023-02-04 18:54:25');
INSERT INTO `sys_role_user` VALUES (16, 2, 5, 1, '2023-02-18 00:32:15', '2023-02-18 00:32:15');
INSERT INTO `sys_role_user` VALUES (18, 1, 6, 1, '2023-02-18 00:32:51', '2023-02-18 00:32:51');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `real_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `head_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  `gender` tinyint(3) UNSIGNED NULL DEFAULT NULL COMMENT '性别   0：男   1：女    2：保密',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `super_admin` tinyint(3) UNSIGNED NULL DEFAULT NULL COMMENT '超级管理员   0：否   1：是',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态  0：停用   1：正常',
  `creator` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updater` bigint(20) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_username`(`username`) USING BTREE,
  INDEX `idx_create_date`(`create_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '超级管理员', NULL, 0, 'linzee666@163.com', '13631122211', 5, 1, 1, 1, '2022-11-08 17:38:27', 1, '2022-11-08 17:38:27');
INSERT INTO `sys_user` VALUES (2, 'test', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '普通管理员', '', 0, 'test@163.com', '13631122222', 1, 0, 1, 1, '2022-11-29 18:29:40', 1, '2022-11-29 18:29:40');
INSERT INTO `sys_user` VALUES (5, 'test1', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 'test', NULL, 1, '', '', 3, 0, 1, 1, '2023-01-23 15:57:31', 1, '2023-02-18 00:32:15');
INSERT INTO `sys_user` VALUES (6, 'test2', '60303ae22b998861bce3b28f33eec1be758a213c86c93c076dbe9f558c11c752', 'test2', NULL, 2, '', '', 2, 0, 1, 1, '2023-01-23 15:58:57', 1, '2023-02-18 00:32:51');
INSERT INTO `sys_user` VALUES (8, 'qqq', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 'qqq', NULL, 0, '', '', 5, 0, 1, 1, '2023-01-23 16:08:06', 1, '2023-01-23 16:08:06');

-- ----------------------------
-- Table structure for sys_user_token
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_token`;
CREATE TABLE `sys_user_token`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户token',
  `expire_time` datetime NULL DEFAULT NULL COMMENT '过期时间',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `user_id`(`user_id`) USING BTREE,
  UNIQUE INDEX `token`(`token`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统管理员Token' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_token
-- ----------------------------
INSERT INTO `sys_user_token` VALUES (3, 1, '5eed186dd1ceceff692f09ad949e2f89', '2023-02-19 00:49:19', '2023-02-15 18:51:40', '2023-02-15 18:51:40');
INSERT INTO `sys_user_token` VALUES (4, 6, '6e9cb924c4a6f701e91815c4b654076b', '2023-02-19 00:33:54', '2023-02-18 00:33:54', '2023-02-18 00:33:54');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `mobile` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '手机号',
  `password` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES (1, '13631122222', '13631122222', 'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f', '2022-11-09 01:23:50', '2022-11-09 01:23:50');

-- ----------------------------
-- Table structure for tb_user_token
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_token`;
CREATE TABLE `tb_user_token`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `token` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'token',
  `expire_time` datetime NULL DEFAULT NULL COMMENT '过期时间',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `user_id`(`user_id`) USING BTREE,
  UNIQUE INDEX `token`(`token`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户Token' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user_token
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
