package com.zee.common.utils;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ArrayUtil;
import com.zee.common.exception.ExceptionCast;
import com.zee.common.model.response.CommonCode;
import com.zee.common.model.response.ResultCode;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * 校验工具类
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public class AssertUtil {

    public static void isBlank(String str) {
        if (StringUtils.isBlank(str)) {
            ExceptionCast.cast(CommonCode.INVALID_PARAM);
        }
    }

    public static void isBlank(String str, String params) {
        if (StringUtils.isBlank(str)) {
            ExceptionCast.cast(params + "不能为空");
        }
    }

    public static void isBlank(String str, ResultCode resultCode) {
        if (StringUtils.isBlank(str)) {
            ExceptionCast.cast(resultCode);
        }
    }

    public static void isNull(Object object) {
        if (object == null) {
            ExceptionCast.cast(CommonCode.INVALID_PARAM);
        }
    }

    public static void isNull(Object object, String params) {
        if (object == null) {
            ExceptionCast.cast(params + "不能为空");
        }
    }

    public static void isNull(Object object, ResultCode resultCode) {
        if (object == null) {
            ExceptionCast.cast(resultCode);
        }
    }

    public static void isArrayEmpty(Object[] array, String params) {
        if (ArrayUtil.isEmpty(array)) {
            ExceptionCast.cast(params + " Array不能为空");
        }
    }

    public static void isArrayEmpty(Object[] array, ResultCode resultCode) {
        if (ArrayUtil.isEmpty(array)) {
            ExceptionCast.cast(resultCode);
        }
    }

    public static void isListEmpty(List<?> list, String params) {
        if (CollUtil.isEmpty(list)) {
            ExceptionCast.cast(params + " List不能为空");
        }
    }

    public static void isListEmpty(List<?> list, ResultCode resultCode) {
        if (CollUtil.isEmpty(list)) {
            ExceptionCast.cast(resultCode);
        }
    }

    public static void isMapEmpty(Map map, String params) {
        if (MapUtil.isEmpty(map)) {
            ExceptionCast.cast(params + " Map不能为空");
        }
    }

    public static void isMapEmpty(Map map, ResultCode resultCode) {
        if (MapUtil.isEmpty(map)) {
            ExceptionCast.cast(resultCode);
        }
    }
}