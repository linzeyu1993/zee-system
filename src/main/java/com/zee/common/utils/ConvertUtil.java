package com.zee.common.utils;

import com.zee.common.exception.ExceptionCast;
import com.zee.common.model.response.CommonCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 转换工具类
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
@Slf4j
public class ConvertUtil {

    /**
     * 对象转成目标对象
     *
     * @param source 原始对象
     * @param target 目标对象字节码
     * @return
     */
    public static <T> T sourceToTarget(Object source, Class<T> target) {
        if (source == null) {
            return null;
        }
        T targetObject = null;
        try {
            targetObject = target.newInstance();
            BeanUtils.copyProperties(source, targetObject);
        } catch (Exception e) {
            log.error("convert error ", e);
        }

        return targetObject;
    }

    /**
     * 对象map集合转成目标对象list集合
     *
     * @param sourceList 原始map集合
     * @param target     目标对象list集合字节码
     * @return
     */
    public static <T> List<T> sourceToTarget(Collection<?> sourceList, Class<T> target) {
        if (sourceList == null) {
            return null;
        }

        List targetList = new ArrayList<>(sourceList.size());
        try {
            for (Object source : sourceList) {
                T targetObject = target.newInstance();
                BeanUtils.copyProperties(source, targetObject);
                targetList.add(targetObject);
            }
        } catch (Exception e) {
            log.error("convert error ", e);
        }

        return targetList;
    }

    /**
     * 空字符串转null值，最终返回Integer类型
     * 适用于：前端搜索条件传空，但是后端用hashmap接收强转integer报错的情况，如：“”，1，2，其中空串无法转integer
     * 适用于：前端搜索条件传字符串数值，后端后端用hashmap接收强转类型出错，如：“1”，“2”,其中"1"无法转integer
     *
     * @param obj
     * @return
     */
    public static Integer emptyStringToNull(Object obj) {
        //空值直接返回
        if (obj == null) {
            return null;
        }
        // 只放进int和stirng类型的参数
        if (!(obj instanceof Integer || obj instanceof String)) {
            ExceptionCast.cast(CommonCode.INVALID_PARAM);
        }
        // 空字符转null，如：“” => null
        if (obj instanceof String && StringUtils.isBlank((String) obj)) {
            return null;
        }
        // 非空字符串转数字，如：“1” => 1
        if (obj instanceof String && StringUtils.isNotBlank((String) obj)) {
            return Integer.parseInt((String) obj);
        }
        return (Integer) obj;
    }

}