package com.zee.common.utils;

import java.util.List;

public interface EasyExcelRepeatWritePages<T> {

    /**
     * 获取总页数，因为需要循环读出每一页的数据，所以需要知道总页数
     * @return
     */
    public abstract int getPageSize();

    /**
     * 获取当前页读取的数据
     *
     * @param page 当前页码
     * @return
     */
    public abstract List<T> getCurrentPageData(int page);
}
