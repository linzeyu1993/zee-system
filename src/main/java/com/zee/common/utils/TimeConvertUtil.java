package com.zee.common.utils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * 时间转换工具类
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public class TimeConvertUtil {

    private static ZoneId zoneId = ZoneId.systemDefault();

    /**
     * Date类型转LocalDateTime类型
     *
     * @param date
     * @return
     */
    public static LocalDateTime toLocalDateTime(Date date) {
        Instant instant = date.toInstant();
        return instant.atZone(zoneId).toLocalDateTime();
    }

    /**
     * LocalDateTime类型转Date类型
     *
     * @param localDateTime
     * @return
     */
    public static Date toDate(LocalDateTime localDateTime) {
        ZonedDateTime zdt = localDateTime.atZone(zoneId);
        return Date.from(zdt.toInstant());
    }

    /**
     * localDateTime转毫秒时间戳
     *
     * @param localDateTime
     * @return
     */
    public static Long getMillisTime(LocalDateTime localDateTime) {
        Instant instant = localDateTime.atZone(zoneId).toInstant();
        return Date.from(instant).getTime();
    }
}
