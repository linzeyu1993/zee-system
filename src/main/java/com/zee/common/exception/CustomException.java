package com.zee.common.exception;

import com.zee.common.model.response.ResultCode;

/**
 * 自定义异常类
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public class CustomException extends RuntimeException {

    ResultCode resultCode;

    public CustomException(ResultCode resultCode) {
        this.resultCode = resultCode;
    }

    /**
     * 直接抛出提示信息
     *
     * @param message
     */
    public CustomException(String message) {
        this.resultCode = new ResultCode() {
            @Override
            public int code() {
                return 1001;
            }

            @Override
            public String message() {
                return message;
            }
        };
    }


    public ResultCode getResultCode() {
        return resultCode;
    }


}
