package com.zee.common.exception;


import com.zee.common.model.response.ResultCode;

/**
 * 抛出自定义异常
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public class ExceptionCast {
    public static void cast(ResultCode resultCode) {
        throw new CustomException(resultCode);
    }

    public static void cast(String message) {
        throw new CustomException(message);
    }
}

