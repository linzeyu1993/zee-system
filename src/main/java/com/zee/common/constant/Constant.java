package com.zee.common.constant;

/**
 * 常量
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public interface Constant {
    /**
     * 成功
     */
    int SUCCESS = 1;
    /**
     * 失败
     */
    int FAIL = 0;
    /**
     * 菜单根节点标识
     */
    Integer MENU_ROOT = 0;
    /**
     * 部门根节点标识
     */
    Integer DEPT_ROOT = 0;
    /**
     * 升序
     */
    String ASC = "asc";
    /**
     * 降序
     */
    String DESC = "desc";
    /**
     * 每页显示记录数
     */
    String LIMIT = "limit";
    /**
     * token header
     */
    String TOKEN_HEADER = "token";
    /**
     * 数据权限过滤
     */
    String SQL_FILTER = "sqlFilter";
    /**
     * 云存储配置KEY
     */
    String CLOUD_STORAGE_CONFIG_KEY = "CLOUD_STORAGE_CONFIG_KEY";
}