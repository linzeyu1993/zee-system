package com.zee.common.config;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2WebMvc
@EnableKnife4j
public class Knife4jConfiguration {


    @Bean(value = "adminDoc")
    public Docket adminDoc() {
        //设置token为全局参数
        ParameterBuilder tokenPar = new ParameterBuilder();
        List<Parameter> pars = new ArrayList<>();
        tokenPar.name("token")
                .description("token令牌")
                .defaultValue("")
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .required(false)
                .build();
        pars.add(tokenPar.build());

        Docket docket = new Docket(DocumentationType.SWAGGER_2).enable(true)
                .apiInfo(new ApiInfoBuilder()
                        .description("管理后台api接口文档")
                        .termsOfServiceUrl("http://www.xxx.com/")
                        .version("1.0")
                        .contact(new Contact("LinZee", "", "linzee666@163.com"))
                        .title("管理后台api接口文档")
                        .build())
                //分组名称
                .groupName("管理后台api")
//                .ignoredParameterTypes(UserTokenClass.class)
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.zee.admin.controller"))
                .paths(PathSelectors.any())
                .build()//设置token全局参数
                .globalOperationParameters(pars);
        return docket;
    }

    @Bean(value = "homeDoc")
    public Docket homeDoc() {
        //设置token为全局参数
        ParameterBuilder tokenPar = new ParameterBuilder();
        List<Parameter> pars = new ArrayList<>();
        tokenPar.name("token")
                .description("token令牌")
                .defaultValue("")
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .required(false)
                .build();
        pars.add(tokenPar.build());

        Docket docket = new Docket(DocumentationType.SWAGGER_2).enable(true)
                .apiInfo(new ApiInfoBuilder()
                        .description("前台api接口文档")
                        .termsOfServiceUrl("http://www.xxx.com/")
                        .version("1.0")
                        .contact(new Contact("LinZee", "", "linzee666@163.com"))
                        .title("前台api接口文档")
                        .build())
                //分组名称
                .groupName("前台api")
//                .ignoredParameterTypes(UserTokenClass.class)
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.zee.home.controller"))
                .paths(PathSelectors.any())
                .build()
                //设置token全局参数
                .globalOperationParameters(pars);
        return docket;
    }

}
