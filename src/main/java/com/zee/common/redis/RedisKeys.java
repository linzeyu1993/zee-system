package com.zee.common.redis;

/**
 * 获取redis缓存key对应的值
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public class RedisKeys {
    /**
     * 系统参数Key
     */
    public static String getSysParamsKey() {
        return "sys:params";
    }

    /**
     * 验证码Key
     */
    public static String getCaptchaKey(String uuid) {
        return "sys:captcha:" + uuid;
    }

    /**
     * api登录用户Key
     */
    public static String getHomeUserKey(Integer id) {
        return "api:home:user:" + id;
    }

    /**
     * 管理后台登录用户key
     */
    public static String getAdminUserKey(Integer userId) {
        return "sys:admin:user:" + userId;
    }

    /**
     * 管理后台登录用户token
     */
    public static String getAdminUserToken(String token) {
        return "sys:admin:token:" + token;
    }

    /**
     * sign签名key
     */
    public static String getSignKey(String key, String nonce) {
        return "api:sign:" + key + ":" + nonce;
    }
}
