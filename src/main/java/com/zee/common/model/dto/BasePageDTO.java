package com.zee.common.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 分页基础dto，需要继承数据过滤
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
@Data
public class BasePageDTO extends DataScopeDTO {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "当前页不能为空")
    @Min(value = 1, message = "当前页最小为1")
    @ApiModelProperty(notes = "当前页", required = true, example = "1")
    private Integer page = 1;

    @NotNull(message = "分页大小不能为空")
    @Min(value = 1, message = "分页大小最小为1")
    @ApiModelProperty(notes = "分页大小", required = true, example = "10")
    private Integer pageSize = 10;
}
