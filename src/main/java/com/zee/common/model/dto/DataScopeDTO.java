package com.zee.common.model.dto;

import com.zee.admin.interceptor.DataScope;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 后台admin模块，数据过滤用
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
@Data
public class DataScopeDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    //由aop注入，数据过滤用
    @ApiModelProperty(notes = "数据过滤sql", hidden = true)
    private DataScope dataScope;
}
