package com.zee.common.model.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Desc
 * @Date 2021/5/21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "响应")
public class Result {
    @ApiModelProperty(notes = "响应的状态码")
    private Integer code;
    @ApiModelProperty(notes = "响应的信息")
    private String msg;
    @ApiModelProperty(notes = "响应的数据")
    private Object data;

    public static Result ok() {
        return new Result(CommonCode.SUCCESS.code(), CommonCode.SUCCESS.message(), null);
    }

    public static Result ok(Object data) {
        return new Result(CommonCode.SUCCESS.code(), CommonCode.SUCCESS.message(), data);
    }

    public static Result ok(ResultCode resultCode) {
        return new Result(resultCode.code(), resultCode.message(), null);
    }

    public static Result ok(ResultCode resultCode, Object data) {
        return new Result(resultCode.code(), resultCode.message(), data);
    }

    public static Result error() {
        return new Result(CommonCode.FAIL.code(), CommonCode.FAIL.message(), null);
    }

    public static Result error(ResultCode resultCode) {
        return new Result(resultCode.code(), resultCode.message(), null);
    }

    public static Result error(ResultCode resultCode, Object data) {
        return new Result(resultCode.code(), resultCode.message(), data);
    }

    public static Result error(Integer code, String message) {
        return new Result(code, message, null);
    }

    public static Result error(Integer code, String message, Object data) {
        return new Result(code, message, data);
    }
}
