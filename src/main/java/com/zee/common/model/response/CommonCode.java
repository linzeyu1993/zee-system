package com.zee.common.model.response;

public enum CommonCode implements ResultCode {

    SERVER_ERROR(9999, "抱歉，系统繁忙，请稍后重试！"),

    SUCCESS(200, "操作成功"),
    FAIL(1001, "操作失败"),
    INVALID_PARAM(1002, "参数错误"),
    LOGIN_FAIL(1003, "登录失败"),
    ACCOUNT_PASSWORD_ERROR(1004, "帐号或密码错误"),
    ORIGINAL_PASSWORD_ERROR(1005, "原密码错误"),
    ACCOUNT_DISABLE(1006, "帐号不可用"),


    UNAUTHORIZED(40001, "没有权限访问"),
    ACCOUNT_LOCK(40088, "帐号被锁定");

    //操作代码
    int code;
    //提示信息
    String message;

    private CommonCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public int code() {
        return this.code;
    }

    @Override
    public String message() {
        return this.message;
    }
}
