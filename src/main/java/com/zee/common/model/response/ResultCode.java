package com.zee.common.model.response;

/**
 * 1000～1999 区间表示参数错误
 * 2000～2999 区间表示用户错误
 * 3000～3999 区间表示接口异常
 */
public interface ResultCode {
    //操作代码
    int code();

    //提示信息
    String message();

}
