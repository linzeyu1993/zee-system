package com.zee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.zee.*"})
public class ZeeApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZeeApplication.class, args);
    }
}