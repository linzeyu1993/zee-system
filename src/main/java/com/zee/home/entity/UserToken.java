package com.zee.home.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zee.common.entity.BaseHomeEntity;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 用户Token
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
@Data
@TableName("tb_user_token")
public class UserToken extends BaseHomeEntity {
    private static final long serialVersionUID = 1L;
    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 用户token
     */
    private String token;
    /**
     * 过期时间
     */
    private LocalDateTime expireTime;

}