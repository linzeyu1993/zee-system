package com.zee.home.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zee.common.entity.BaseHomeEntity;
import lombok.Data;

/**
 * 用户
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
@Data
@TableName("tb_user")
public class User extends BaseHomeEntity {
    private static final long serialVersionUID = 1L;
    /**
     * 用户名
     */
    private String username;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 密码
     */
    @JsonIgnore
    private String password;

}