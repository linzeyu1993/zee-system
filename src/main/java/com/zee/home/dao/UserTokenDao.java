package com.zee.home.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zee.home.entity.UserToken;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserTokenDao extends BaseMapper<UserToken> {

    UserToken getByToken(String token);

    UserToken getByUserId(Integer userId);
}
