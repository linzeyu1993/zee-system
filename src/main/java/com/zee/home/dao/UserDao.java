package com.zee.home.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zee.home.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserDao extends BaseMapper<User> {

    User getUserByMobile(String mobile);

    User getUserByUserId(Integer userId);
}
