package com.zee.home.annotation;

import java.lang.annotation.*;

/**
 * 登录效验
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Login {
}
