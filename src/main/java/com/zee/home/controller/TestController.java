package com.zee.home.controller;

import com.zee.common.model.response.Result;
import com.zee.home.annotation.Login;
import com.zee.home.annotation.LoginUser;
import com.zee.home.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 测试接口
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
@RestController
@RequestMapping("/api/test")
@Api(tags = "测试接口")
public class TestController {

    @Login
    @GetMapping("userInfo")
    @ApiOperation(value = "获取用户信息", response = User.class)
    //@ApiImplicitParam(paramType = "header", name = "token", required = true, value = "token令牌")
    public Result userInfo(@ApiIgnore @LoginUser User user) {
        return Result.ok(user);
    }

    @Login
    @GetMapping("userId")
    @ApiOperation("获取用户ID")
    //@ApiImplicitParam(paramType = "header", name = "token", required = true, value = "token令牌")
    public Result userInfo(@ApiIgnore @RequestAttribute("userId") Integer userId) {
        return Result.ok(userId);
    }

    @GetMapping("notToken")
    @ApiOperation("忽略Token验证测试")
    public Result notToken() {
        return Result.ok("无需token也能访问。。。");
    }

}