package com.zee.home.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zee.home.entity.User;
import com.zee.home.model.dto.LoginDTO;

import java.util.Map;

public interface IUserService extends IService<User> {

    User getByMobile(String mobile);

    User getUserByUserId(Integer userId);

    /**
     * 用户登录
     *
     * @param dto 登录表单
     * @return 返回登录信息
     */
    Map<String, Object> login(LoginDTO dto);
}
