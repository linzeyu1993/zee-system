package com.zee.home.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zee.home.entity.UserToken;

public interface IUserTokenService extends IService<UserToken> {

    UserToken getByToken(String token);

    /**
     * 生成token
     *
     * @param userId 用户ID
     * @return 返回token信息
     */
    UserToken createToken(Integer userId);

    /**
     * 设置token过期
     *
     * @param userId 用户ID
     */
    void expireToken(Integer userId);

}
