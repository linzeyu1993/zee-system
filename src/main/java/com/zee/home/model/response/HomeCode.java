package com.zee.home.model.response;

import com.zee.common.model.response.ResultCode;

public enum HomeCode implements ResultCode {

    TOKEN_NOT_EMPTY(2001, "token不能为空"),
    TOKEN_INVALID(2002, "非法的token"),
    USER_INVALID(2003, "非法用户或用户不存在");

    //操作代码
    int code;
    //提示信息
    String message;

    private HomeCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public int code() {
        return this.code;
    }

    @Override
    public String message() {
        return this.message;
    }
}
