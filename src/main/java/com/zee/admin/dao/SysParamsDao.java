package com.zee.admin.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zee.admin.entity.SysParams;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysParamsDao extends BaseMapper<SysParams> {
    /**
     * 根据参数编码获得参数
     *
     * @param paramCode 参数编码
     * @return
     */
    SysParams getParamsByCode(@Param("paramCode") String paramCode);

    /**
     * 根据参数编码，查询value
     *
     * @param paramCode 参数编码
     * @return 参数值
     */
    String getValueByCode(@Param("paramCode") String paramCode);

    /**
     * 获取参数编码列表
     *
     * @param ids ids
     * @return 返回参数编码列表
     */
    List<String> getParamCodeList(Integer[] ids);

    /**
     * 根据参数编码，更新value
     *
     * @param paramCode  参数编码
     * @param paramValue 参数值
     * @return
     */
    int updateValueByCode(@Param("paramCode") String paramCode, @Param("paramValue") String paramValue);
}
