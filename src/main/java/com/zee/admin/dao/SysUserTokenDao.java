package com.zee.admin.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zee.admin.entity.SysUserToken;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface SysUserTokenDao extends BaseMapper<SysUserToken> {

    SysUserToken getByToken(@Param("token") String token);

    SysUserToken getByUserId(@Param("userId") Integer userId);

    void updateToken(@Param("userId") Integer userId, @Param("token") String token);
}
