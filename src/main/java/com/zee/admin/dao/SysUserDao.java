package com.zee.admin.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zee.admin.entity.SysUser;
import com.zee.admin.model.vo.SysUserVO;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

public interface SysUserDao extends BaseMapper<SysUser> {

    /**
     * 分页列表
     *
     * @param page        分页对象
     * @param queryParams 查询参数
     * @return
     */
    Page<SysUserVO> page(@Param("page") Page page, @Param("params") HashMap<String, Object> queryParams);

    /**
     * 列表
     *
     * @param params 查询参数
     * @return
     */
    List<SysUserVO> list(@Param("params") HashMap<String, Object> params);

    /**
     * 查询用户信息
     *
     * @param id 用户Id
     * @return
     */
    SysUserVO getById(@Param("id") Integer id);

    /**
     * 根据用户名称查询用户
     *
     * @param username 用户名称
     * @return
     */
    SysUser getByUsername(@Param("username") String username);

    /**
     * 更新用户密码
     *
     * @param id          用户Id
     * @param newPassword 新密码
     * @return
     */
    int updatePassword(@Param("id") Integer id, @Param("newPassword") String newPassword);

    /**
     * 根据部门ID，查询用户数
     *
     * @param deptId 部门id
     * @return
     */
    int getCountByDeptId(@Param("deptId") Integer deptId);

    /**
     * 根据部门ID,查询用户ID列表
     *
     * @param deptIdList 部门ids
     * @return
     */
    List<Integer> getUserIdListByDeptId(@Param("deptIdList") List<Integer> deptIdList);

    /**
     * 查询超级管理员数量
     *
     * @param idList 用户ids
     * @return
     */
    Integer getCountBySuperAdmin(@Param("idList") List<Integer> idList);

    /**
     * 测试
     *
     * @param map
     * @return
     */
    List<SysUser> test(@Param("map") HashMap<String, Object> map);
}