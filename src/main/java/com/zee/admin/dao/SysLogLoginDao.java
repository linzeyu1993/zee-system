package com.zee.admin.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zee.admin.entity.SysLogLogin;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysLogLoginDao extends BaseMapper<SysLogLogin> {

}
