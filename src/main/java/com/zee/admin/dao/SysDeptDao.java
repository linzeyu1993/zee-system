package com.zee.admin.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zee.admin.entity.SysDept;
import com.zee.admin.model.vo.SysDeptVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

@Mapper
public interface SysDeptDao extends BaseMapper<SysDept> {

    /**
     * 部门列表
     *
     * @param queryParams 查询参数
     * @return
     */
    List<SysDeptVO> getList(@Param("queryParams") HashMap<String, Object> queryParams);

    /**
     * 获取部门信息
     *
     * @param id 部门Id
     * @return
     */
    SysDeptVO info(@Param("id") Integer id);

    /**
     * 获取所有部门的id、pid列表
     *
     * @return
     */
    List<SysDept> getIdAndPidList();

    /**
     * 根据部门ID，获取所有子部门ID列表
     *
     * @param id 部门ID
     * @return
     */
    List<Integer> getSubDeptIdList(@Param("id") String id);
}