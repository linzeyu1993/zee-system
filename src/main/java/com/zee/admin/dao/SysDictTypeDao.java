package com.zee.admin.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zee.admin.entity.SysDictType;
import com.zee.admin.model.vo.DictTypeVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SysDictTypeDao extends BaseMapper<SysDictType> {

    /**
     * 字典类型列表
     *
     * @return
     */
    List<DictTypeVO> getDictTypeList();

}
