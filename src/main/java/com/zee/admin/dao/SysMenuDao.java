package com.zee.admin.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zee.admin.entity.SysMenu;
import com.zee.admin.model.vo.SysMenuVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysMenuDao extends BaseMapper<SysMenu> {

    /**
     * 根据id查询信息
     *
     * @param id
     * @return
     */
    SysMenuVO getById(@Param("id") Integer id);

    /**
     * 查询所有菜单列表
     *
     * @param menuType 菜单类型
     * @return
     */
    List<SysMenuVO> getMenuList(@Param("menuType") Integer menuType);

    /**
     * 查询用户菜单列表
     *
     * @param userId   用户ＩＤ
     * @param menuType 菜单类型
     * @return
     */
    List<SysMenuVO> getUserMenuList(@Param("userId") Integer userId, @Param("menuType") Integer menuType);

    /**
     * 查询用户权限列表
     *
     * @param userId 用户ID
     * @return
     */
    List<String> getUserPermissionsList(@Param("userId") Integer userId);

    /**
     * 查询所有权限列表
     *
     * @return
     */
    List<String> getPermissionsList();

    /**
     * 根据父菜单，查询子菜单
     *
     * @param pid 父菜单ID
     * @return
     */
    List<SysMenu> getListPid(@Param("pid") Integer pid);

}
