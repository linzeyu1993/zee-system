package com.zee.admin.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zee.admin.entity.SysRole;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysRoleDao extends BaseMapper<SysRole> {

}
