package com.zee.admin.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zee.admin.entity.SysRoleMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysRoleMenuDao extends BaseMapper<SysRoleMenu> {

    /**
     * 根据角色ID，获取菜单ID列表
     *
     * @param roleId 角色Id
     * @return
     */
    List<Integer> getMenuIdList(@Param("roleId") Integer roleId);

    /**
     * 根据角色id，删除角色菜单关系
     *
     * @param roleIds 角色ids
     */
    void deleteByRoleIds(Integer[] roleIds);

    /**
     * 根据菜单id，删除角色菜单关系
     *
     * @param menuId 菜单id
     */
    void deleteByMenuId(@Param("menuId") Integer menuId);
}
