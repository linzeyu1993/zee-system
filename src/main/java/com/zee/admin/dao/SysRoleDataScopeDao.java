package com.zee.admin.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zee.admin.entity.SysRoleDataScope;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysRoleDataScopeDao extends BaseMapper<SysRoleDataScope> {

    /**
     * 根据角色ID，获取部门ID列表
     *
     * @param roleId 角色id
     * @return
     */
    List<Integer> getDeptIdList(@Param("roleId") Integer roleId);

    /**
     * 获取用户的部门数据权限列表
     *
     * @param userId 用户id
     * @return
     */
    List<Integer> getDataScopeList(@Param("userId") Integer userId);

    /**
     * 根据角色id，删除角色数据权限关系
     *
     * @param roleIds 角色ids
     */
    void deleteByRoleIds(Integer[] roleIds);
}