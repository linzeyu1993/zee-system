package com.zee.admin.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zee.admin.entity.SysDictData;
import com.zee.admin.model.vo.DictDataVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SysDictDataDao extends BaseMapper<SysDictData> {

    /**
     * 字典数据列表
     *
     * @return
     */
    List<DictDataVO> getDictDataList();
}
