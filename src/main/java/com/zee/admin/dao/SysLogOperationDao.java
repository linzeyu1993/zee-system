package com.zee.admin.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zee.admin.entity.SysLogOperation;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysLogOperationDao extends BaseMapper<SysLogOperation> {

}
