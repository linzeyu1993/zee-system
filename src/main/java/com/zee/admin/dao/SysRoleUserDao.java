package com.zee.admin.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zee.admin.entity.SysRoleUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysRoleUserDao extends BaseMapper<SysRoleUser> {

    /**
     * 根据角色ids，删除角色用户关系
     *
     * @param roleIds 角色ids
     */
    void deleteByRoleIds(Integer[] roleIds);

    /**
     * 根据用户id，删除角色用户关系
     *
     * @param userIds 用户ids
     */
    void deleteByUserIds(Integer[] userIds);

    /**
     * 角色ID列表
     *
     * @param userId 用户ID
     * @return
     */
    List<Integer> getRoleIdList(@Param("userId") Integer userId);
}