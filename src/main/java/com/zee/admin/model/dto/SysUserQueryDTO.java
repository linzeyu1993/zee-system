package com.zee.admin.model.dto;

import com.zee.common.model.dto.BasePageDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SysUserQueryDTO extends BasePageDTO {
    @ApiModelProperty(notes = "筛选|用户名")
    private String username;

    @ApiModelProperty(notes = "筛选|性别，0男|1女|2保密")
    private Integer gender;

    @ApiModelProperty(notes = "筛选|部门ID")
    private Integer deptId;
}
