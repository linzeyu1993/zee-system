package com.zee.admin.model.dto;

import com.zee.common.model.dto.BasePageDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SysLogOperationQueryDTO extends BasePageDTO {
    @ApiModelProperty(notes = "筛选|操作状态, 0失败|1成功")
    private Integer status;
}
