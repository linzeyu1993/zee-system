package com.zee.admin.model.dto;

import com.zee.common.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel(value = "参数管理")
public class SysParamsDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @NotNull(message = "ID不能为空", groups = UpdateGroup.class)
    private Integer id;

    @ApiModelProperty(value = "参数编码")
    @NotNull(message = "参数编码不能为空")
    private String paramCode;

    @ApiModelProperty(value = "参数值")
    @NotNull(message = "参数值不能为空")
    private String paramValue;

    @ApiModelProperty(value = "备注")
    private String remark;

}
