package com.zee.admin.model.dto;

import com.zee.common.model.dto.DataScopeDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "登录表单")
public class SysLoginDTO extends DataScopeDTO {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户名", required = true)
    @NotBlank(message = "用户名不能为空")
    private String username;

    @ApiModelProperty(value = "密码", required = true)
    @NotBlank(message = "密码不能为空")
    private String password;

    @ApiModelProperty(value = "验证码，验证码功能开启时不能为空")
    private String captcha;

    @ApiModelProperty(value = "唯一标识（uuid），验证码功能开启时不能为空")
    private String uuid;

}