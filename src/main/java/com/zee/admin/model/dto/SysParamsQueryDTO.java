package com.zee.admin.model.dto;

import com.zee.common.model.dto.BasePageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "参数查询")
public class SysParamsQueryDTO extends BasePageDTO {
    @ApiModelProperty(notes = "筛选|参数编码")
    private String paramCode;
}
