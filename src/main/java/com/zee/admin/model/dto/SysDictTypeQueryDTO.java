package com.zee.admin.model.dto;

import com.zee.common.model.dto.BasePageDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SysDictTypeQueryDTO extends BasePageDTO {
    @ApiModelProperty(notes = "筛选|字典名称")
    private String dictName;

    @ApiModelProperty(notes = "筛选|字典类型")
    private String dictType;
}
