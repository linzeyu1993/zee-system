package com.zee.admin.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class SysUserPasswordDTO implements Serializable {
    @ApiModelProperty(notes = "旧密码", required = true)
    @NotBlank(message = "")
    private String password;

    @ApiModelProperty(notes = "新密码", required = true)
    private String newPassword;
}
