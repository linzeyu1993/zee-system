package com.zee.admin.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "参数查询")
public class SysMenuQueryDTO {
    @ApiModelProperty(notes = "筛选|菜单类型 0：菜单 1：按钮  null：全部")
    private Integer type;
}
