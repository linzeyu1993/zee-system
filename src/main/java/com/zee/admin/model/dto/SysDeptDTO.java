package com.zee.admin.model.dto;

import com.zee.common.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class SysDeptDTO implements Serializable {
    @ApiModelProperty(value = "ID")
    @NotNull(message = "ID不能为空", groups = UpdateGroup.class)
    private Integer id;

    @ApiModelProperty(value = "上级ID", required = true)
    @NotNull(message = "上级ID不能为空")
    private Integer pid;

    @ApiModelProperty(value = "部门名称", required = true)
    @NotBlank(message = "部门名称不能为空")
    private String name;

    @ApiModelProperty(value = "排序")
    private Integer sort;
}
