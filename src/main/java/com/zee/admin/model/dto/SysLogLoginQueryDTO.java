package com.zee.admin.model.dto;

import com.zee.common.model.dto.BasePageDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SysLogLoginQueryDTO extends BasePageDTO {
    @ApiModelProperty(notes = "筛选|用户名")
    private String creatorName;

    @ApiModelProperty(notes = "筛选|状态, 0失败|1成功|2帐号已锁定")
    private Integer status;

    @ApiModelProperty(notes = "筛选|用户操作, 0登录|1登出")
    private Integer operation;
}
