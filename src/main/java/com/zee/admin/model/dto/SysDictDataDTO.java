package com.zee.admin.model.dto;

import com.zee.common.validator.group.AddGroup;
import com.zee.common.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel(value = "字典数据")
public class SysDictDataDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "id")
    @NotNull(message = "id不能为空", groups = UpdateGroup.class)
    private Integer id;

    @ApiModelProperty(notes = "字典类型id")
    @NotNull(message = "字典类型id不能为空", groups = AddGroup.class)
    private Integer dictTypeId;

    @ApiModelProperty(value = "字典标签")
    private String dictLabel;

    @ApiModelProperty(value = "字典值")
    private String dictValue;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "排序")
    @Min(value = 0, message = "排序字段需要大于0")
    private Integer sort;
}