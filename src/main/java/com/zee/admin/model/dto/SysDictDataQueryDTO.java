package com.zee.admin.model.dto;

import com.zee.common.model.dto.BasePageDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SysDictDataQueryDTO extends BasePageDTO {
    @ApiModelProperty(notes = "字典类型id")
    @NotNull(message = "字典类型id不能为空")
    private Integer dictTypeId;

    @ApiModelProperty(notes = "筛选|字典id")
    private String dictLabel;

    @ApiModelProperty(notes = "筛选|字典id")
    private String dictValue;
}
