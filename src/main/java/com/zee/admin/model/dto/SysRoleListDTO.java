package com.zee.admin.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SysRoleListDTO {
    @ApiModelProperty(notes = "筛选|角色名称")
    private String roleName;
}
