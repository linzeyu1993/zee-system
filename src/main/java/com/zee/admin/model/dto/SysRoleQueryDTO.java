package com.zee.admin.model.dto;

import com.zee.common.model.dto.BasePageDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SysRoleQueryDTO extends BasePageDTO {
    @ApiModelProperty(notes = "筛选|角色名称")
    private String roleName;
}
