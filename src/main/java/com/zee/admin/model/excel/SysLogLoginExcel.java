package com.zee.admin.model.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.zee.admin.model.excel.converter.StatusConverter;
import com.zee.admin.model.excel.converter.SysLogLoginOperationConverter;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class SysLogLoginExcel implements Serializable {
    @ExcelProperty(value = "操作类型", converter = SysLogLoginOperationConverter.class)
    private Integer operation;
    @ExcelProperty(value = "状态", converter = StatusConverter.class)
    private Integer status;
    @ExcelProperty("用户代理")
    private String userAgent;
    @ExcelProperty("操作IP")
    private String ip;
    @ExcelProperty("用户名")
    private String creatorName;
    @ExcelProperty("创建时间")
    private LocalDateTime createTime;
}
