package com.zee.admin.model.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.zee.admin.model.excel.converter.StatusConverter;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class SysLogOperationExcel implements Serializable {
    @ExcelProperty("用户操作")
    private String operation;
    @ExcelProperty("请求URI")
    private String requestUri;
    @ExcelProperty("请求方式")
    private String requestMethod;
    @ExcelProperty("请求参数")
    private String requestParams;
    @ExcelProperty("请求时长(毫秒)")
    private Integer requestTime;
    @ExcelProperty("用户代理")
    private String userAgent;
    @ExcelProperty("操作IP")
    private String ip;
    @ExcelProperty(value = "状态", converter = StatusConverter.class)
    private Integer status;
    @ExcelProperty("用户名")
    private String creatorName;
    @ExcelProperty("创建时间")
    private LocalDateTime createTime;
}
