package com.zee.admin.model.excel;


import com.alibaba.excel.annotation.ExcelProperty;
import com.zee.admin.model.excel.converter.SexConverter;
import com.zee.admin.model.excel.converter.SysUserStatusConverter;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class SysUserExcel implements Serializable {
    @ExcelProperty("用户名")
    private String username;
    @ExcelProperty("姓名")
    private String realName;
    @ExcelProperty(value = "性别", converter = SexConverter.class)
    private Integer gender;
    @ExcelProperty("邮箱")
    private String email;
    @ExcelProperty("手机号")
    private String mobile;
    @ExcelProperty("部门名称")
    private String deptName;
    @ExcelProperty(value = "状态", converter = SysUserStatusConverter.class)
    private Integer status;
    @ExcelProperty("备注")
    private String remark;
    @ExcelProperty("创建时间")
    private LocalDateTime createTime;
}