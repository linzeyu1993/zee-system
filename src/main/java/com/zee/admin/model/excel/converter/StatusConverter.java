package com.zee.admin.model.excel.converter;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.data.WriteCellData;

/**
 * 用户状态转换器
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public class StatusConverter implements Converter<Integer> {
    @Override
    public Class supportJavaTypeKey()
    {
        return Integer.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey()
    {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public WriteCellData convertToExcelData(WriteConverterContext<Integer> context)
    {
        String text = "男";
        switch (context.getValue()) {
            case 0:
                text = "失败";
                break;
            case 1:
                text = "成功";
                break;
            case 2:
                text = "账号已锁定";
                break;
        }
        return new WriteCellData(text);
    }

    @Override
    public Integer convertToJavaData(ReadConverterContext context)
    {
        int value = 0;
        String str = context.getReadCellData().getStringValue();
        switch (str) {
            case "失败":
                value = 0;
                break;
            case "成功":
                value = 1;
                break;
            case "账号已锁定":
                value = 2;
                break;
        }
        return value;
    }
}
