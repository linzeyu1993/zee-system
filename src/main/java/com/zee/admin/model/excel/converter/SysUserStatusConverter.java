package com.zee.admin.model.excel.converter;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.data.WriteCellData;

/**
 * 用户状态转换器
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public class SysUserStatusConverter implements Converter<Integer> {
    @Override
    public Class supportJavaTypeKey()
    {
        return Integer.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey()
    {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public WriteCellData convertToExcelData(WriteConverterContext<Integer> context)
    {
        return new WriteCellData(0 == context.getValue() ? "停用" : "正常");
    }

    @Override
    public Integer convertToJavaData(ReadConverterContext context)
    {
        return "停用".equals(context.getReadCellData().getStringValue()) ? 0 : 1;
    }
}
