package com.zee.admin.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class DictTypeVO {
    @JsonIgnore
    private Integer id;
    private String dictType;
    private List<DictDataVO> dataList = new ArrayList<>();
}
