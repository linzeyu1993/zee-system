package com.zee.admin.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class DictDataVO {
    @JsonIgnore
    private Integer dictTypeId;
    private String dictLabel;
    private String dictValue;
}
