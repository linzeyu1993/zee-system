package com.zee.admin.model.response;

import com.zee.common.model.response.ResultCode;

public enum AdminCode implements ResultCode {
    TOKEN_EMPTY(4001, "token为空"),
    TOKEN_INVALID(4003, "非法的token"),
    USER_INVALID(4004, "非法用户或用户不存在"),
    SUPERIOR_DEPT_ERROR(4005, "设置部门错误"),
    DEPT_SUB_DELETE_ERROR(4006, "删除部门错误"),
    CAN_NOT_DELETE_SUPER_ADMIN(4007, "不能删除超级管理员"),
    SUPERIOR_MENU_ERROR(4008, "更新菜单错误"),
    SUB_MENU_EXIST(4009, "子菜单已存在"),
    DATA_SCOPE_PARAMS_ERROR(4010, "数据权限不足"),
    CAPTCHA_IS_NOT_ENABLED(4011, "验证码功能未开启"),
    PARAMS_CODE_IS_EXIST(4012, "参数编码已存在"),
    USER_NAME_IS_EXIST(4013, "用户名已存在"),

    CAPTCHA_ERROR(4022, "验证码错误");

    //操作代码
    int code;
    //提示信息
    String message;

    private AdminCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public int code() {
        return this.code;
    }

    @Override
    public String message() {
        return this.message;
    }
}