package com.zee.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zee.admin.annotation.LogOperation;
import com.zee.admin.entity.SysRole;
import com.zee.admin.model.dto.SysRoleDTO;
import com.zee.admin.model.dto.SysRoleListDTO;
import com.zee.admin.model.dto.SysRoleQueryDTO;
import com.zee.admin.model.vo.SysRoleVO;
import com.zee.admin.service.ISysRoleDataScopeService;
import com.zee.admin.service.ISysRoleMenuService;
import com.zee.admin.service.ISysRoleService;
import com.zee.common.model.response.Result;
import com.zee.common.validator.group.UpdateGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 角色管理
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
@RestController
@RequestMapping("/sys/role")
@Api(tags = "角色管理")
public class SysRoleController {
    @Autowired
    private ISysRoleService sysRoleService;
    @Autowired
    private ISysRoleMenuService sysRoleMenuService;
    @Autowired
    private ISysRoleDataScopeService sysRoleDataScopeService;

    @PostMapping("page")
    @ApiOperation("分页列表")
    @RequiresPermissions("sys:role:page")
    public Result page(@Validated @RequestBody SysRoleQueryDTO dto) {
        Page<SysRole> page = sysRoleService.page(dto);
        return Result.ok(page);
    }

    @PostMapping("list")
    @ApiOperation("列表")
    @RequiresPermissions("sys:role:list")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response = SysRoleVO.class)
    })
    public Result list(@Validated @RequestBody SysRoleListDTO dto) {
        List<SysRole> data = sysRoleService.list(dto);

        return Result.ok(data);
    }

    @GetMapping("info")
    @ApiOperation("信息")
    @RequiresPermissions("sys:role:info")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response = SysRoleVO.class)
    })
    public Result info(@RequestParam("id") Integer id) {
        SysRoleVO sysRoleVO = sysRoleService.info(id);
        //查询角色对应的菜单
        List<Integer> menuIdList = sysRoleMenuService.getMenuIdList(id);
        sysRoleVO.setMenuIdList(menuIdList);
        //查询角色对应的数据权限
        List<Integer> deptIdList = sysRoleDataScopeService.getDeptIdList(id);
        sysRoleVO.setDeptIdList(deptIdList);
        return Result.ok(sysRoleVO);
    }

    @PostMapping("save")
    @ApiOperation("保存")
    @LogOperation("保存角色")
    @RequiresPermissions("sys:role:save")
    public Result save(@Validated @RequestBody SysRoleDTO dto) {
        sysRoleService.save(dto);
        return Result.ok();
    }

    @PostMapping("update")
    @ApiOperation("修改")
    @LogOperation("修改角色")
    @RequiresPermissions("sys:role:update")
    public Result update(@Validated(UpdateGroup.class) @RequestBody SysRoleDTO dto) {
        sysRoleService.update(dto);
        return Result.ok();
    }

    @PostMapping("delete")
    @ApiOperation("删除")
    @LogOperation("删除角色")
    @RequiresPermissions("sys:role:delete")
    public Result delete(@Validated @RequestBody Integer[] ids) {
        sysRoleService.delete(ids);
        return Result.ok();
    }
}