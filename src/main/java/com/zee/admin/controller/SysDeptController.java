package com.zee.admin.controller;

import com.zee.admin.annotation.LogOperation;
import com.zee.admin.model.dto.SysDeptDTO;
import com.zee.admin.model.vo.SysDeptVO;
import com.zee.admin.service.ISysDeptService;
import com.zee.common.model.response.Result;
import com.zee.common.validator.group.UpdateGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 部门管理
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
@RestController
@RequestMapping("/sys/dept")
@Api(tags = "部门管理")
public class SysDeptController {
    @Autowired
    private ISysDeptService sysDeptService;

    @PostMapping("list")
    @ApiOperation("部门管理列表")
    @RequiresPermissions("sys:dept:list")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response = SysDeptVO.class)
    })
    public Result list() {
        List<SysDeptVO> list = sysDeptService.getList();
        return Result.ok(list);
    }

    @GetMapping("info")
    @ApiOperation("信息")
    @RequiresPermissions("sys:dept:info")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response = SysDeptVO.class)
    })
    public Result info(@RequestParam("id") Integer id) {
        SysDeptVO sysDeptVO = sysDeptService.info(id);
        return Result.ok(sysDeptVO);
    }

    @PostMapping("save")
    @ApiOperation("保存")
    @LogOperation("保存部门信息")
    @RequiresPermissions("sys:dept:save")
    public Result save(@Validated @RequestBody SysDeptDTO dto) {
        sysDeptService.save(dto);
        return Result.ok();
    }

    @PostMapping("update")
    @ApiOperation("修改")
    @LogOperation("修改部门信息")
    @RequiresPermissions("sys:dept:update")
    public Result update(@Validated({UpdateGroup.class}) @RequestBody SysDeptDTO dto) {
        sysDeptService.update(dto);
        return Result.ok();
    }

    @PostMapping("delete")
    @ApiOperation("删除")
    @LogOperation("删除部门信息")
    @RequiresPermissions("sys:dept:delete")
    public Result delete(@RequestBody Integer[] ids) {
        for (Integer id : ids) {
            sysDeptService.delete(id);
        }
        return Result.ok();
    }

}