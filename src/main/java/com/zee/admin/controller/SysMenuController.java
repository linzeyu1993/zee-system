package com.zee.admin.controller;

import com.zee.admin.annotation.LogOperation;
import com.zee.admin.entity.SysMenu;
import com.zee.admin.enums.MenuTypeEnum;
import com.zee.admin.model.dto.SysMenuDTO;
import com.zee.admin.model.dto.SysMenuQueryDTO;
import com.zee.admin.model.response.AdminCode;
import com.zee.admin.model.vo.SysMenuVO;
import com.zee.admin.service.IShiroService;
import com.zee.admin.service.ISysMenuService;
import com.zee.admin.shiro.AdminSecurityUser;
import com.zee.admin.shiro.AdminUserDetail;
import com.zee.common.model.response.Result;
import com.zee.common.validator.group.UpdateGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * 菜单管理
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
@RestController
@RequestMapping("/sys/menu")
@Api(tags = "菜单管理")
public class SysMenuController {
    @Autowired
    private ISysMenuService sysMenuService;
    @Autowired
    private IShiroService shiroService;

    @GetMapping("nav")
    @ApiOperation("导航")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response = SysMenuVO.class)
    })
    public Result nav() {
        AdminUserDetail user = AdminSecurityUser.getUser();
        List<SysMenuVO> list = sysMenuService.getUserMenuList(user, MenuTypeEnum.MENU.value());

        return Result.ok(list);
    }

    @GetMapping("permissions")
    @ApiOperation("权限标识")
    public Result permissions() {
        AdminUserDetail user = AdminSecurityUser.getUser();
        Set<String> set = shiroService.getUserPermissions(user);

        return Result.ok(set);
    }

    @PostMapping("list")
    @ApiOperation("列表")
    @RequiresPermissions("sys:menu:list")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response = SysMenuVO.class)
    })
    public Result list(@RequestBody SysMenuQueryDTO dto) {
        List<SysMenuVO> list = sysMenuService.getAllMenuList(dto.getType());

        return Result.ok(list);
    }

    @GetMapping("info")
    @ApiOperation("信息")
    @RequiresPermissions("sys:menu:info")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response = SysMenuVO.class)
    })
    public Result info(@RequestParam("id") Integer id) {
        SysMenuVO data = sysMenuService.info(id);

        return Result.ok(data);
    }

    @PostMapping("save")
    @ApiOperation("保存")
    @LogOperation("保存菜单")
    @RequiresPermissions("sys:menu:save")
    public Result save(@Validated @RequestBody SysMenuDTO dto) {
        sysMenuService.save(dto);

        return Result.ok();
    }

    @PostMapping("update")
    @ApiOperation("修改")
    @LogOperation("修改菜单")
    @RequiresPermissions("sys:menu:update")
    public Result update(@Validated(UpdateGroup.class) @RequestBody SysMenuDTO dto) {
        sysMenuService.update(dto);

        return Result.ok();
    }

    @GetMapping("delete")
    @ApiOperation("删除")
    @LogOperation("删除菜单")
    @RequiresPermissions("sys:menu:delete")
    public Result delete(@RequestParam("id") Integer id) {
        //判断是否有子菜单或按钮
        List<SysMenu> list = sysMenuService.getListPid(id);
        if (list.size() > 0) {
            return Result.error(AdminCode.SUB_MENU_EXIST);
        }
        sysMenuService.delete(id);

        return Result.ok();
    }

    @GetMapping("select")
    @ApiOperation("角色菜单权限")
    @RequiresPermissions("sys:menu:select")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response = SysMenuVO.class)
    })
    public Result select() {
        AdminUserDetail user = AdminSecurityUser.getUser();
        List<SysMenuVO> list = sysMenuService.getUserMenuList(user, null);

        return Result.ok(list);
    }
}