package com.zee.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zee.admin.annotation.LogOperation;
import com.zee.admin.entity.SysDictType;
import com.zee.admin.model.dto.SysDictTypeDTO;
import com.zee.admin.model.dto.SysDictTypeQueryDTO;
import com.zee.admin.model.vo.DictTypeVO;
import com.zee.admin.service.ISysDictTypeService;
import com.zee.common.model.response.Result;
import com.zee.common.utils.ConvertUtil;
import com.zee.common.validator.group.UpdateGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 字典类型
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
@RestController
@RequestMapping("sys/dict/type")
@Api(tags = "字典类型")
public class SysDictTypeController {
    @Autowired
    private ISysDictTypeService sysDictTypeService;

    @PostMapping("page")
    @ApiOperation("分页列表")
    @RequiresPermissions("sys:dict:page")
    public Result page(@Validated @RequestBody SysDictTypeQueryDTO dto) {
        //字典类型
        Page<SysDictType> page = sysDictTypeService.page(dto);

        return Result.ok(page);
    }

    @GetMapping("info")
    @ApiOperation("信息")
    @RequiresPermissions("sys:dict:info")
    public Result info(@RequestParam Integer id) {
        SysDictType sysDictType = sysDictTypeService.getById(id);

        return Result.ok(sysDictType);
    }

    @PostMapping("save")
    @ApiOperation("保存")
    @LogOperation("保存字典类型")
    @RequiresPermissions("sys:dict:save")
    public Result save(@Validated @RequestBody SysDictTypeDTO dto) {
        SysDictType sysDictType = ConvertUtil.sourceToTarget(dto, SysDictType.class);
        sysDictTypeService.save(sysDictType);

        return Result.ok();
    }

    @PostMapping("update")
    @ApiOperation("修改")
    @LogOperation("修改字典类型")
    @RequiresPermissions("sys:dict:update")
    public Result update(@Validated(UpdateGroup.class) @RequestBody SysDictTypeDTO dto) {
        SysDictType sysDictType = ConvertUtil.sourceToTarget(dto, SysDictType.class);
        sysDictTypeService.updateById(sysDictType);

        return Result.ok();
    }

    @PostMapping("delete")
    @ApiOperation("删除")
    @LogOperation("删除字典类型")
    @RequiresPermissions("sys:dict:delete")
    public Result delete(@RequestBody Integer[] ids) {
        sysDictTypeService.removeByIds(Arrays.asList(ids));

        return Result.ok();
    }

    @GetMapping("all")
    @ApiOperation("所有字典数据")
    public Result all() {
        List<DictTypeVO> list = sysDictTypeService.getAllList();

        return Result.ok(list);
    }

}