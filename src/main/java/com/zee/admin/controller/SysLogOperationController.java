package com.zee.admin.controller;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.zee.admin.annotation.LogOperation;
import com.zee.admin.entity.SysLogOperation;
import com.zee.admin.model.dto.SysLogOperationQueryDTO;
import com.zee.admin.model.excel.SysLogOperationExcel;
import com.zee.admin.service.ISysLogOperationService;
import com.zee.common.model.response.Result;
import com.zee.common.utils.ConvertUtil;
import com.zee.common.utils.EasyExcelRepeatWritePages;
import com.zee.common.utils.EasyExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;

/**
 * 操作日志相关接口
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
@RestController
@RequestMapping("sys/logOperation")
@Api(tags = "操作日志")
public class SysLogOperationController {
    @Autowired
    private ISysLogOperationService sysLogOperationService;

    @PostMapping("page")
    @ApiOperation(value = "分页列表")
    @ApiOperationSupport(order = 10)
    @RequiresPermissions("sys:logOperation:page")
    public Result page(@Validated @RequestBody SysLogOperationQueryDTO dto) {
        Page<SysLogOperation> page = sysLogOperationService.page(dto);
        return Result.ok(page);
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出操作")
    @RequiresPermissions("sys:logOperation:export")
    public void export(@RequestParam HashMap<String, Object> params, HttpServletResponse response) {
        String filename = "export_" + System.currentTimeMillis();
        //简单导出示例，适合5000内数据量
        //List<SysLogOperation> sysLogOperationList = sysLogOperationService.list(params);
        //List<SysLogOperationExcel> list = ConvertUtil.sourceToTarget(sysLogOperationList, SysLogOperationExcel.class);
        //EasyExcelUtil.simpleWrite(response, filename, list, "操作日志", SysLogOperationExcel.class);

        //循环导出示例，循环分页读取数据
        EasyExcelUtil.repeatWrite(response, filename, "操作日志",
                SysLogOperationExcel.class, new EasyExcelRepeatWritePages<SysLogOperationExcel>() {
                    //每次读多少条
                    final int pageSize = 1000;
                    @Override
                    public int getPageSize() {
                        return (int) Math.ceil(sysLogOperationService.count(params) * 1.0 / pageSize);
                    }

                    @Override
                    public List<SysLogOperationExcel> getCurrentPageData(int page) {
                        //hashmap转成dto，service少写一个方法
                        SysLogOperationQueryDTO dto =
                                BeanUtil.fillBeanWithMap(params, new SysLogOperationQueryDTO(), true, true);
                        dto.setPage(page);
                        dto.setPageSize(pageSize);
                        Page<SysLogOperation> data = sysLogOperationService.page(dto);
                        return ConvertUtil.sourceToTarget(data.getRecords(), SysLogOperationExcel.class);
                    }
                });
    }

}