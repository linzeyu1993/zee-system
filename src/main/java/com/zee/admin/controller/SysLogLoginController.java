package com.zee.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.zee.admin.annotation.LogOperation;
import com.zee.admin.entity.SysLogLogin;
import com.zee.admin.model.dto.SysLogLoginQueryDTO;
import com.zee.admin.model.excel.SysLogLoginExcel;
import com.zee.admin.service.ISysLogLoginService;
import com.zee.common.model.response.Result;
import com.zee.common.utils.ConvertUtil;
import com.zee.common.utils.EasyExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;

/**
 * 登录日志相关接口
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
@RestController
@RequestMapping("sys/logLogin")
@Api(tags = "登录日志")
public class SysLogLoginController {
    @Autowired
    ISysLogLoginService sysLogLoginService;

    @PostMapping("page")
    @ApiOperation(value = "分页列表")
    @ApiOperationSupport(order = 10)
    @RequiresPermissions("sys:logLogin:page")
    public Result page(@Validated @RequestBody SysLogLoginQueryDTO dto) {
        Page<SysLogLogin> page = sysLogLoginService.page(dto);
        return Result.ok(page);
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出登录日志")
    @RequiresPermissions("sys:logLogin:export")
    public void export(@RequestParam HashMap<String, Object> params, HttpServletResponse response) {
        List<SysLogLogin> sysLogLoginList = sysLogLoginService.list(params);
        List<SysLogLoginExcel> list = ConvertUtil.sourceToTarget(sysLogLoginList, SysLogLoginExcel.class);
        String filename = "export_" + System.currentTimeMillis();
        EasyExcelUtil.simpleWrite(response, filename, list, "登录日志", SysLogLoginExcel.class);
    }
}
