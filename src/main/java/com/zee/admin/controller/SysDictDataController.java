package com.zee.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zee.admin.annotation.LogOperation;
import com.zee.admin.entity.SysDictData;
import com.zee.admin.model.dto.SysDictDataDTO;
import com.zee.admin.model.dto.SysDictDataQueryDTO;
import com.zee.admin.service.ISysDictDataService;
import com.zee.common.model.response.Result;
import com.zee.common.utils.ConvertUtil;
import com.zee.common.validator.group.AddGroup;
import com.zee.common.validator.group.UpdateGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * 字典数据
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
@RestController
@RequestMapping("sys/dict/data")
@Api(tags = "字典数据")
public class SysDictDataController {
    @Autowired
    private ISysDictDataService sysDictDataService;

    @PostMapping("page")
    @ApiOperation("分页列表")
    @RequiresPermissions("sys:dict:page")
    public Result page(@Validated @RequestBody SysDictDataQueryDTO dto) {
        //字典类型
        Page<SysDictData> page = sysDictDataService.page(dto);

        return Result.ok(page);
    }

    @GetMapping("info")
    @ApiOperation("信息")
    @RequiresPermissions("sys:dict:info")
    public Result info(@RequestParam Integer id) {
        SysDictData data = sysDictDataService.getById(id);

        return Result.ok(data);
    }

    @PostMapping("save")
    @ApiOperation("保存")
    @LogOperation("保存字典数据")
    @RequiresPermissions("sys:dict:save")
    public Result save(@Validated(AddGroup.class) @RequestBody SysDictDataDTO dto) {
        SysDictData sysDictData = ConvertUtil.sourceToTarget(dto, SysDictData.class);
        sysDictDataService.save(sysDictData);

        return Result.ok();
    }

    @PostMapping("update")
    @ApiOperation("修改")
    @LogOperation("修改字典数据")
    @RequiresPermissions("sys:dict:update")
    public Result update(@Validated(UpdateGroup.class) @RequestBody SysDictDataDTO dto) {
        SysDictData sysDictData = ConvertUtil.sourceToTarget(dto, SysDictData.class);
        sysDictDataService.updateById(sysDictData);

        return Result.ok();
    }

    @PostMapping("delete")
    @ApiOperation("删除")
    @LogOperation("删除字典数据")
    @RequiresPermissions("sys:dict:delete")
    public Result delete(@RequestBody Integer[] ids) {
        sysDictDataService.removeByIds(Arrays.asList(ids));

        return Result.ok();
    }

}