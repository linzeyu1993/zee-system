package com.zee.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zee.admin.annotation.LogOperation;
import com.zee.admin.entity.SysParams;
import com.zee.admin.model.dto.SysParamsDTO;
import com.zee.admin.model.dto.SysParamsQueryDTO;
import com.zee.admin.model.excel.SysParamsExcel;
import com.zee.admin.service.ISysParamsService;
import com.zee.common.model.response.Result;
import com.zee.common.utils.EasyExcelUtil;
import com.zee.common.validator.group.UpdateGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * 非系统参数管理
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
@RestController
@RequestMapping("sys/params")
@Api(tags = "非系统参数管理")
public class SysParamsController {
    @Autowired
    private ISysParamsService sysParamsService;

    @PostMapping("page")
    @ApiOperation("分页列表")
    @RequiresPermissions("sys:params:page")
    public Result page(@Validated @RequestBody SysParamsQueryDTO dto) {
        Page<SysParams> page = sysParamsService.page(dto);

        return Result.ok(page);
    }

    @GetMapping("info")
    @ApiOperation("信息")
    @RequiresPermissions("sys:params:info")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response = SysParams.class)
    })
    public Result info(@RequestParam("id") Integer id) {
        SysParams data = sysParamsService.getById(id);

        return Result.ok(data);
    }

    @PostMapping("save")
    @ApiOperation("保存")
    @LogOperation("保存系统参数")
    @RequiresPermissions("sys:params:save")
    public Result save(@Validated @RequestBody SysParamsDTO dto) {
        sysParamsService.save(dto);

        return Result.ok();
    }

    @PostMapping("update")
    @ApiOperation("修改")
    @LogOperation("修改系统参数")
    @RequiresPermissions("sys:params:update")
    public Result update(@Validated(UpdateGroup.class) @RequestBody SysParamsDTO dto) {
        sysParamsService.update(dto);

        return Result.ok();
    }

    @PostMapping("delete")
    @ApiOperation("删除")
    @LogOperation("删除系统参数")
    @RequiresPermissions("sys:params:delete")
    public Result delete(@RequestBody Integer[] ids) {
        sysParamsService.removeByIds(Arrays.asList(ids));

        return Result.ok();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出系统参数")
    @RequiresPermissions("sys:params:export")
    public void export(@RequestParam HashMap<String, Object> params, HttpServletResponse response) {
        List<SysParamsExcel> list = sysParamsService.exportList(params);
        String filename = "export_" + System.currentTimeMillis();
        EasyExcelUtil.simpleWrite(response, filename, list, "系统参数", SysParamsExcel.class);
    }

}