package com.zee.admin.shiro;

import com.zee.admin.model.response.AdminCode;
import com.zee.common.exception.ExceptionCast;

import java.security.MessageDigest;
import java.util.UUID;

/**
 * 生成token
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public class AdminTokenGenerator {

    public static String generateValue() {
        return generateValue(UUID.randomUUID().toString());
    }

    private static final char[] HEX_CODE = "0123456789abcdef".toCharArray();

    public static String toHexString(byte[] data) {
        if (data == null) {
            return null;
        }
        StringBuilder r = new StringBuilder(data.length * 2);
        for (byte b : data) {
            r.append(HEX_CODE[(b >> 4) & 0xF]);
            r.append(HEX_CODE[(b & 0xF)]);
        }
        return r.toString();
    }

    public static String generateValue(String param) {
        try {
            MessageDigest algorithm = MessageDigest.getInstance("MD5");
            algorithm.reset();
            algorithm.update(param.getBytes());
            byte[] messageDigest = algorithm.digest();
            return toHexString(messageDigest);
        } catch (Exception e) {
            ExceptionCast.cast(AdminCode.TOKEN_INVALID);
        }
        return null;
    }
}
