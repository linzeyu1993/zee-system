package com.zee.admin.shiro;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * token
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public class AdminShiroToken implements AuthenticationToken {
    private String token;

    public AdminShiroToken(String token) {
        this.token = token;
    }

    @Override
    public String getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }
}
