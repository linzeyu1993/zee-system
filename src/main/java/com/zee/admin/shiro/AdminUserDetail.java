package com.zee.admin.shiro;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 登录管理员用户信息
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
@Data
public class AdminUserDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private String username;
    private String realName;
    private String headUrl;
    private Integer gender;
    private String email;
    private String mobile;
    private Integer deptId;
    private String password;
    private Integer status;
    private Integer superAdmin;
    /**
     * 部门数据权限
     */
    private List<Integer> deptIdList;

}