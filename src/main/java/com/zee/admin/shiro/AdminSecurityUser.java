package com.zee.admin.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

/**
 * 获取管理用户信息
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public class AdminSecurityUser {

    public static Subject getSubject() {
        try {
            return SecurityUtils.getSubject();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取用户信息
     */
    public static AdminUserDetail getUser() {
        Subject subject = getSubject();
        if (subject == null) {
            return new AdminUserDetail();
        }

        AdminUserDetail user = (AdminUserDetail) subject.getPrincipal();
        if (user == null) {
            return new AdminUserDetail();
        }

        return user;
    }

    /**
     * 获取用户ID
     */
    public static Integer getUserId() {
        return getUser().getId();
    }

    /**
     * 获取部门ID
     */
    public static Integer getDeptId() {
        return getUser().getDeptId();
    }
}