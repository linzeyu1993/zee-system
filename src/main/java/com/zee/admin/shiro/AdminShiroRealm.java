package com.zee.admin.shiro;

import com.zee.admin.entity.SysUser;
import com.zee.admin.entity.SysUserToken;
import com.zee.admin.model.response.AdminCode;
import com.zee.admin.service.IShiroService;
import com.zee.common.exception.ExceptionCast;
import com.zee.common.model.response.CommonCode;
import com.zee.common.utils.ConvertUtil;
import com.zee.common.utils.TimeConvertUtil;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

/**
 * realm认证和授权
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
@Component
public class AdminShiroRealm extends AuthorizingRealm {
    @Autowired
    private IShiroService shiroService;

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof AdminShiroToken;
    }

    /**
     * 授权(验证权限时调用)
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        AdminUserDetail user = (AdminUserDetail) principals.getPrimaryPrincipal();

        //用户权限列表
        Set<String> permsSet = shiroService.getUserPermissions(user);

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.setStringPermissions(permsSet);
        return info;
    }

    /**
     * 认证(登录时调用)
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        String accessToken = (String) token.getPrincipal();

        //根据accessToken，查询用户信息
        SysUserToken sysUserToken = shiroService.getByToken(accessToken);
        //token失效
        if (sysUserToken == null || TimeConvertUtil.getMillisTime(sysUserToken.getExpireTime()) < System.currentTimeMillis()) {
            ExceptionCast.cast(AdminCode.TOKEN_INVALID);
        }

        //查询用户信息
        SysUser sysUser = shiroService.getUser(sysUserToken.getUserId());

        //转换成UserDetail对象
        AdminUserDetail userDetail = ConvertUtil.sourceToTarget(sysUser, AdminUserDetail.class);

        //获取用户对应的部门数据权限
        List<Integer> deptIdList = shiroService.getDataScopeList(userDetail.getId());
        userDetail.setDeptIdList(deptIdList);

        //账号锁定
        if (userDetail.getStatus() == 0) {
            ExceptionCast.cast(CommonCode.ACCOUNT_LOCK);
        }

        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(userDetail, accessToken, getName());
        return info;
    }

}