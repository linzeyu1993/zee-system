package com.zee.admin.enums;

/**
 * 登录操作枚举
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public enum LoginOperationEnum {
    /**
     * 用户登录
     */
    LOGIN(0),
    /**
     * 用户退出
     */
    LOGOUT(1);

    private int value;

    LoginOperationEnum(int value) {
        this.value = value;
    }

    public int value() {
        return this.value;
    }
}