package com.zee.admin.enums;

/**
 * 用户状态枚举
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public enum UserStatusEnum {
    /**
     * 禁用
     */
    DISABLE(0),
    /**
     * 可用
     */
    ENABLED(1);

    private int value;

    UserStatusEnum(int value) {
        this.value = value;
    }

    public int value() {
        return this.value;
    }
}
