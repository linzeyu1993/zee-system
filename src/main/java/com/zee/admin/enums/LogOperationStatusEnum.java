package com.zee.admin.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.Getter;

/**
 * 操作日志表的状态枚举
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
@Getter
//@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum LogOperationStatusEnum {
    /**
     * 失败
     */
    FAIL(0, "失败"),
    /**
     * 成功
     */
    SUCCESS(1, "成功");


    @EnumValue
    private final Integer value;

    private final String text;

    LogOperationStatusEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    //@JsonCreator
    //public static SysLogOperationStatusEnum parse(String value) {
    //    for (SysLogOperationStatusEnum enumValue : values())
    //        if (enumValue.name().equalsIgnoreCase(value))
    //            return enumValue;
    //
    //    throw new EnumConstantNotPresentException(SysLogOperationStatusEnum.class, value);
    //}

    public Integer value() {
        return this.value;
    }

    public String text() {
        return this.text;
    }
}