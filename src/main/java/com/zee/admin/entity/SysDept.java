package com.zee.admin.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zee.common.entity.BaseAdminEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 部门管理
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_dept")
public class SysDept extends BaseAdminEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 上级ID
     */
    private Integer pid;
    /**
     * 部门名称
     */
    private String name;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Integer updater;

}