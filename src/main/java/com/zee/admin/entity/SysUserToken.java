package com.zee.admin.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 系统用户Token
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
@Data
@TableName("sys_user_token")
public class SysUserToken implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 逻辑删除字段，0不存在，1存在
     */
//    private Integer exist;
    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 用户token
     */
    private String token;
    /**
     * 过期时间
     */
    private LocalDateTime expireTime;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

}