package com.zee.admin.annotation;

import java.lang.annotation.*;

/**
 * 操作日志注解
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LogOperation {

    String value() default "";
}
