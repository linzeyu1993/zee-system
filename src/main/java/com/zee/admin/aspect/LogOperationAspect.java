package com.zee.admin.aspect;

import com.alibaba.fastjson.JSONObject;
import com.zee.admin.annotation.LogOperation;
import com.zee.admin.entity.SysLogOperation;
import com.zee.admin.enums.LogOperationStatusEnum;
import com.zee.admin.service.ISysLogOperationService;
import com.zee.admin.shiro.AdminSecurityUser;
import com.zee.admin.shiro.AdminUserDetail;
import com.zee.common.utils.HttpContextUtil;
import com.zee.common.utils.IpUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * 操作日志，切面处理类
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
@Aspect
@Component
public class LogOperationAspect {
    @Autowired
    private ISysLogOperationService sysLogOperationService;

    @Pointcut("@annotation(com.zee.admin.annotation.LogOperation)")
    public void logPointCut() {

    }

    @Around("logPointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        long beginTime = System.currentTimeMillis();
        try {
            //执行方法
            Object result = point.proceed();
            //执行时长(毫秒)
            long time = System.currentTimeMillis() - beginTime;
            //保存日志
            saveLog(point, time, LogOperationStatusEnum.SUCCESS.getValue());
            return result;
        } catch (Exception e) {
            //执行时长(毫秒)
            long time = System.currentTimeMillis() - beginTime;
            //保存日志
            saveLog(point, time, LogOperationStatusEnum.FAIL.getValue());

            throw e;
        }
    }

    private void saveLog(ProceedingJoinPoint joinPoint, long time, Integer status) throws Exception {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = joinPoint.getTarget().getClass().getDeclaredMethod(signature.getName(), signature.getParameterTypes());
        LogOperation annotation = method.getAnnotation(LogOperation.class);

        SysLogOperation log = new SysLogOperation();
        if (annotation != null) {
            //注解上的描述
            log.setOperation(annotation.value());
        }

        //登录用户信息
        AdminUserDetail user = AdminSecurityUser.getUser();
        log.setCreatorName(user.getUsername());
        log.setStatus(status);
        log.setRequestTime((int) time);

        //请求相关信息
        HttpServletRequest request = HttpContextUtil.getHttpServletRequest();
        log.setIp(IpUtil.getIpAddr(request));
        log.setUserAgent(request.getHeader(HttpHeaders.USER_AGENT));
        log.setRequestUri(request.getRequestURI());
        log.setRequestMethod(request.getMethod());

        //请求参数
        Object[] args = joinPoint.getArgs();
        try {
            String params = JSONObject.toJSONString(args[0]);
            log.setRequestParams(params);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //保存到DB
        sysLogOperationService.save(log);
    }
}