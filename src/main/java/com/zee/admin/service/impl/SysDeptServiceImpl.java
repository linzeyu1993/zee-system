package com.zee.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zee.admin.dao.SysDeptDao;
import com.zee.admin.dao.SysUserDao;
import com.zee.admin.entity.SysDept;
import com.zee.admin.enums.SuperAdminEnum;
import com.zee.admin.model.dto.SysDeptDTO;
import com.zee.admin.model.response.AdminCode;
import com.zee.admin.model.vo.SysDeptVO;
import com.zee.admin.service.ISysDeptService;
import com.zee.admin.shiro.AdminSecurityUser;
import com.zee.admin.shiro.AdminUserDetail;
import com.zee.common.constant.Constant;
import com.zee.common.exception.ExceptionCast;
import com.zee.common.utils.ConvertUtil;
import com.zee.common.utils.TreeUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SysDeptServiceImpl extends ServiceImpl<SysDeptDao, SysDept> implements ISysDeptService {
    @Autowired
    private SysUserDao sysUserDao;

    @Override
    public List<SysDeptVO> getList() {
        HashMap<String, Object> queryParams = new HashMap<>();
        //普通管理员，只能查询所属部门及子部门的数据
        AdminUserDetail user = AdminSecurityUser.getUser();
        if (user.getSuperAdmin() == SuperAdminEnum.NO.value()) {
            queryParams.put("deptIdList", getSubDeptIdList(user.getDeptId(), true));
        }
        //查询部门列表
        List<SysDeptVO> sysDeptList = baseMapper.getList(queryParams);
        //构建树形
        return TreeUtil.build(sysDeptList);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SysDeptVO info(Integer id) {
        return baseMapper.info(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(SysDeptDTO dto) {
        SysDept sysDept = ConvertUtil.sourceToTarget(dto, SysDept.class);
        this.save(sysDept);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(SysDeptDTO dto) {
        SysDept sysDept = ConvertUtil.sourceToTarget(dto, SysDept.class);

        //上级部门不能为自身
        if (sysDept.getId().equals(sysDept.getPid())) {
            ExceptionCast.cast((AdminCode.SUPERIOR_DEPT_ERROR));
        }

        //上级部门不能为下级部门
        List<Integer> subDeptList = getSubDeptIdList(sysDept.getId(), true);
        if (subDeptList.contains(sysDept.getPid())) {
            ExceptionCast.cast((AdminCode.SUPERIOR_DEPT_ERROR));
        }
        this.updateById(sysDept);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        //判断是否有子部门
        List<Integer> subList = getSubDeptIdList(id, true);
        if (subList.size() > 1) {
            ExceptionCast.cast((AdminCode.DEPT_SUB_DELETE_ERROR));
        }

        //判断部门下面是否有用户
        int count = sysUserDao.getCountByDeptId(id);
        if (count > 0) {
            ExceptionCast.cast((AdminCode.DEPT_SUB_DELETE_ERROR));
        }

        //删除
        this.removeById(id);
    }

    @Override
    public List<Integer> getSubDeptIdList(Integer id, boolean isAddSelf) {
        List<SysDept> sysDeptList = this.list();
        //转换实体
        List<SysDeptVO> sysDeptVOList = ConvertUtil.sourceToTarget(sysDeptList, SysDeptVO.class);

        List<Integer> data = new ArrayList<>();
        //构建树形
        List<SysDeptVO> subDeptList = TreeUtil.build(sysDeptVOList, id);
        //是否添加自身部门id
        if (isAddSelf) {
            data.add(id);
        }
        //添加下级所有部门id
        this.findSubDeptChildrenId(subDeptList, data);

        return data;
    }

    /**
     * 递归树形，返回树形所有id值
     *
     * @param subDeptList
     * @param data
     */
    public void findSubDeptChildrenId(List<SysDeptVO> subDeptList, List<Integer> data) {
        for (SysDeptVO sysDeptVO : subDeptList) {
            data.add(sysDeptVO.getId());
            if (sysDeptVO.getChildren() == null) {
                continue;
            }
            this.findSubDeptChildrenId(sysDeptVO.getChildren(), data);
        }
    }

    /**
     * 获取所有上级部门ID
     *
     * @param pid 上级ID
     */
    private String getPidList(Integer pid) {
        //顶级部门，无上级部门
        if (Constant.DEPT_ROOT.equals(pid)) {
            return Constant.DEPT_ROOT + "";
        }

        //所有部门的id、pid列表
        List<SysDept> deptList = baseMapper.getIdAndPidList();

        //list转map
        Map<Integer, SysDept> map = new HashMap<>(deptList.size());
        for (SysDept entity : deptList) {
            map.put(entity.getId(), entity);
        }

        //递归查询所有上级部门ID列表
        List<Integer> pidList = new ArrayList<>();
        getPidTree(pid, map, pidList);

        return StringUtils.join(pidList, ",");
    }

    private void getPidTree(Integer pid, Map<Integer, SysDept> map, List<Integer> pidList) {
        //顶级部门，无上级部门
        if (Constant.DEPT_ROOT.equals(pid)) {
            return;
        }

        //上级部门存在
        SysDept parent = map.get(pid);
        if (parent != null) {
            getPidTree(parent.getPid(), map, pidList);
        }

        pidList.add(pid);
    }
}
