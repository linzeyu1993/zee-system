package com.zee.admin.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zee.admin.dao.SysRoleUserDao;
import com.zee.admin.entity.SysRoleUser;
import com.zee.admin.service.ISysRoleUserService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SysRoleUserServiceImpl extends ServiceImpl<SysRoleUserDao, SysRoleUser> implements ISysRoleUserService {

    @Override
    public void saveRoleUser(Integer userId, List<Integer> roleIdList) {
        //先删除角色用户关系
        deleteByUserIds(new Integer[]{userId});

        //用户没有一个角色权限的情况
        if (CollUtil.isEmpty(roleIdList)) {
            return;
        }

        ArrayList<SysRoleUser> sysRoleUsersList = new ArrayList<>();
        //保存角色用户关系
        for (Integer roleId : roleIdList) {
            SysRoleUser sysRoleUser = new SysRoleUser();
            sysRoleUser.setUserId(userId);
            sysRoleUser.setRoleId(roleId);

            sysRoleUsersList.add(sysRoleUser);
        }
        //保存
        this.saveBatch(sysRoleUsersList);
    }

    @Override
    public void deleteByRoleIds(Integer[] roleIds) {
        baseMapper.deleteByRoleIds(roleIds);
    }

    @Override
    public void deleteByUserIds(Integer[] userIds) {
        baseMapper.deleteByUserIds(userIds);
    }

    @Override
    public List<Integer> getRoleIdList(Integer userId) {

        return baseMapper.getRoleIdList(userId);
    }
}