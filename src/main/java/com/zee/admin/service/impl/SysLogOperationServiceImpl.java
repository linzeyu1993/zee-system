package com.zee.admin.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zee.admin.dao.SysLogOperationDao;
import com.zee.admin.entity.SysLogOperation;
import com.zee.admin.model.dto.SysLogOperationQueryDTO;
import com.zee.admin.service.ISysLogOperationService;
import com.zee.common.utils.ConvertUtil;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SysLogOperationServiceImpl extends ServiceImpl<SysLogOperationDao, SysLogOperation> implements ISysLogOperationService {

    @Override
    public Page<SysLogOperation> page(SysLogOperationQueryDTO dto) {
        Map<String, Object> params = BeanUtil.beanToMap(dto);
        return baseMapper.selectPage(new Page<>(dto.getPage(), dto.getPageSize()), getWrapper(params));
    }

    @Override
    public List<SysLogOperation> list(Map<String, Object> params) {
        return baseMapper.selectList(getWrapper(params));
    }

    @Override
    public Integer count(HashMap<String, Object> params) {
        Long count = baseMapper.selectCount(getWrapper(params));
        return Math.toIntExact(count);
    }

    private LambdaQueryWrapper<SysLogOperation> getWrapper(Map<String, Object> params) {
        Integer status = ConvertUtil.emptyStringToNull(params.get("status"));

        LambdaQueryWrapper<SysLogOperation> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(status != null, SysLogOperation::getStatus, status);
        wrapper.orderByDesc(SysLogOperation::getCreateTime);

        return wrapper;
    }
}