package com.zee.admin.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zee.admin.dao.SysDictDataDao;
import com.zee.admin.entity.SysDictData;
import com.zee.admin.model.dto.SysDictDataQueryDTO;
import com.zee.admin.service.ISysDictDataService;
import com.zee.common.utils.ConvertUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class SysDictDataServiceImpl extends ServiceImpl<SysDictDataDao, SysDictData> implements ISysDictDataService {

    @Override
    public Page<SysDictData> page(SysDictDataQueryDTO dto) {
        Map<String, Object> params = BeanUtil.beanToMap(dto);
        return baseMapper.selectPage(new Page<>(dto.getPage(), dto.getPageSize()), getWrapper(params));
    }

    private LambdaQueryWrapper<SysDictData> getWrapper(Map<String, Object> params) {
        Integer dictTypeId = ConvertUtil.emptyStringToNull(params.get("status"));
        String dictLabel = (String) params.get("dictLabel");
        String dictValue = (String) params.get("dictValue");

        LambdaQueryWrapper<SysDictData> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysDictData::getDictTypeId, dictTypeId);
        wrapper.like(StringUtils.isNotBlank(dictLabel), SysDictData::getDictLabel, dictLabel);
        wrapper.like(StringUtils.isNotBlank(dictValue), SysDictData::getDictValue, dictValue);

        return wrapper;
    }

}