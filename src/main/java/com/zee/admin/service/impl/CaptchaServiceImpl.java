package com.zee.admin.service.impl;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.wf.captcha.SpecCaptcha;
import com.wf.captcha.base.Captcha;
import com.zee.admin.service.ICaptchaService;
import com.zee.common.redis.RedisKeys;
import com.zee.common.redis.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Service
public class CaptchaServiceImpl implements ICaptchaService {
    @Value("${zee.redis.open: false}")
    private boolean isOpenRedis;
    /**
     * 过期时间常量，单位秒
     */
    private final Integer EXPIRE_TIME = 5 * 60;
    /**
     * Local Cache  5分钟过期
     */
    Cache<String, String> localCache = CacheBuilder.newBuilder().maximumSize(1000).expireAfterAccess(EXPIRE_TIME, TimeUnit.SECONDS).build();

    @Autowired
    private RedisUtil redisUtil;

    @Override
    public void create(HttpServletResponse response, String uuid) throws IOException {
        response.setContentType("image/gif");
        response.setHeader("Pragma", "No-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);

        //生成验证码
        SpecCaptcha captcha = new SpecCaptcha(150, 40);
        captcha.setLen(5);
        captcha.setCharType(Captcha.TYPE_DEFAULT);
        captcha.out(response.getOutputStream());

        //保存到缓存
        setCache(uuid, captcha.text());
    }

    @Override
    public boolean validate(String uuid, String code) {
        //获取验证码
        String captcha = getCache(uuid);

        //效验成功
        if (code.equalsIgnoreCase(captcha)) {
            return true;
        }

        return false;
    }

    private void setCache(String key, String value) {
        if (this.isOpenRedis) {
            //存进redis
            key = RedisKeys.getCaptchaKey(key);
            redisUtil.set(key, value, EXPIRE_TIME);
        } else {
            localCache.put(key, value);
        }
    }

    private String getCache(String key) {
        if (this.isOpenRedis) {
            //从redis取出
            key = RedisKeys.getCaptchaKey(key);
            String captcha = (String) redisUtil.get(key);
            //删除验证码
            if (captcha != null) {
                redisUtil.del(key);
            }
            return captcha;
        }

        String captcha = localCache.getIfPresent(key);
        //删除验证码
        if (captcha != null) {
            localCache.invalidate(key);
        }
        return captcha;
    }
}