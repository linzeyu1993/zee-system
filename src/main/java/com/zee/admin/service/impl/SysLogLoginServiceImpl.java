package com.zee.admin.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zee.admin.dao.SysLogLoginDao;
import com.zee.admin.entity.SysLogLogin;
import com.zee.admin.model.dto.SysLogLoginQueryDTO;
import com.zee.admin.service.ISysLogLoginService;
import com.zee.common.utils.ConvertUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SysLogLoginServiceImpl extends ServiceImpl<SysLogLoginDao, SysLogLogin> implements ISysLogLoginService {

    @Override
    public Page<SysLogLogin> page(SysLogLoginQueryDTO dto) {
        Map<String, Object> params = BeanUtil.beanToMap(dto);
        return baseMapper.selectPage(new Page<>(dto.getPage(), dto.getPageSize()), getWrapper(params));
    }

    @Override
    public List<SysLogLogin> list(HashMap<String, Object> params) {
        return baseMapper.selectList(getWrapper(params));
    }

    private LambdaQueryWrapper<SysLogLogin> getWrapper(Map<String, Object> params) {
        Integer status = ConvertUtil.emptyStringToNull(params.get("status"));
        String creatorName = (String) params.get("creatorName");
        Integer operation = ConvertUtil.emptyStringToNull(params.get("operation"));

        LambdaQueryWrapper<SysLogLogin> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(status != null, SysLogLogin::getStatus, status);
        wrapper.eq(operation != null, SysLogLogin::getOperation, operation);
        wrapper.like(StringUtils.isNotBlank(creatorName), SysLogLogin::getCreatorName, creatorName);
        wrapper.orderByDesc(SysLogLogin::getCreateTime);

        return wrapper;
    }

}