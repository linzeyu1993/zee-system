package com.zee.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zee.admin.entity.SysMenu;
import com.zee.admin.model.dto.SysMenuDTO;
import com.zee.admin.model.vo.SysMenuVO;
import com.zee.admin.shiro.AdminUserDetail;

import java.util.List;


/**
 * 菜单管理
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public interface ISysMenuService extends IService<SysMenu> {

    SysMenuVO info(Integer id);

    void save(SysMenuDTO dto);

    void update(SysMenuDTO dto);

    void delete(Integer id);

    /**
     * 菜单列表
     *
     * @param menuType 菜单类型
     */
    List<SysMenuVO> getAllMenuList(Integer menuType);

    /**
     * 用户菜单列表
     *
     * @param user     用户
     * @param menuType 菜单类型
     */
    List<SysMenuVO> getUserMenuList(AdminUserDetail user, Integer menuType);

    /**
     * 根据父菜单，查询子菜单
     *
     * @param pid 父菜单ID
     * @return
     */
    List<SysMenu> getListPid(Integer pid);
}
