package com.zee.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zee.admin.entity.SysRoleDataScope;

import java.util.List;

/**
 * 角色数据权限
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public interface ISysRoleDataScopeService extends IService<SysRoleDataScope> {

    /**
     * 根据角色ID，获取部门ID列表
     */
    List<Integer> getDeptIdList(Integer roleId);

    /**
     * 保存或修改
     *
     * @param roleId     角色ID
     * @param deptIdList 部门ID列表
     */
    void saveOrUpdate(Integer roleId, List<Integer> deptIdList);

    /**
     * 根据角色id，删除角色数据权限关系
     *
     * @param roleId 角色ids
     */
    void deleteByRoleIds(Integer[] roleId);
}