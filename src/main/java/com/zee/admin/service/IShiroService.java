package com.zee.admin.service;

import com.zee.admin.entity.SysUser;
import com.zee.admin.entity.SysUserToken;
import com.zee.admin.shiro.AdminUserDetail;

import java.util.List;
import java.util.Set;

/**
 * shiro相关接口
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public interface IShiroService {
    /**
     * 获取用户权限列表
     */
    Set<String> getUserPermissions(AdminUserDetail user);

    SysUserToken getByToken(String token);

    /**
     * 根据用户ID，查询用户
     *
     * @param userId
     */
    SysUser getUser(Integer userId);

    /**
     * 获取用户对应的部门数据权限
     *
     * @param userId 用户ID
     * @return 返回部门ID列表
     */
    List<Integer> getDataScopeList(Integer userId);
}
