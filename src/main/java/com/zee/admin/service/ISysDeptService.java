package com.zee.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zee.admin.entity.SysDept;
import com.zee.admin.model.dto.SysDeptDTO;
import com.zee.admin.model.vo.SysDeptVO;

import java.util.List;

/**
 * 部门管理
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public interface ISysDeptService extends IService<SysDept> {

    List<SysDeptVO> getList();

    SysDeptVO info(Integer id);

    void save(SysDeptDTO dto);

    void update(SysDeptDTO dto);

    void delete(Integer id);

    /**
     * 根据部门ID，获取本部门及子部门ID列表
     *
     * @param id        部门ID
     * @param isAddSelf 是否添加本部门ID
     */
    List<Integer> getSubDeptIdList(Integer id, boolean isAddSelf);
}