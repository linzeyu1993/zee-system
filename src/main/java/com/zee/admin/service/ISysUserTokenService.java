package com.zee.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zee.admin.entity.SysUserToken;

import java.util.Map;

/**
 * 用户Token
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public interface ISysUserTokenService extends IService<SysUserToken> {

    /**
     * 生成token
     *
     * @param userId 用户ID
     */
    Map<String, Object> createToken(Integer userId);

    /**
     * 退出，修改token值
     *
     * @param userId 用户ID
     */
    void logout(Integer userId);

}