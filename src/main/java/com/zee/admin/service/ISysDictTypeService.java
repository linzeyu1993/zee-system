package com.zee.admin.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zee.admin.entity.SysDictType;
import com.zee.admin.model.dto.SysDictTypeQueryDTO;
import com.zee.admin.model.vo.DictTypeVO;

import java.util.List;

/**
 * 数据字典
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public interface ISysDictTypeService extends IService<SysDictType> {

    Page<SysDictType> page(SysDictTypeQueryDTO dto);

    /**
     * 获取所有字典
     */
    List<DictTypeVO> getAllList();

}