package com.zee.admin.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zee.admin.entity.SysDictData;
import com.zee.admin.model.dto.SysDictDataQueryDTO;

/**
 * 数据字典
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public interface ISysDictDataService extends IService<SysDictData> {

    Page<SysDictData> page(SysDictDataQueryDTO dto);

}