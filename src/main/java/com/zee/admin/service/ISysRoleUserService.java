package com.zee.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zee.admin.entity.SysRoleUser;

import java.util.List;

/**
 * 角色用户关系
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public interface ISysRoleUserService extends IService<SysRoleUser> {

    /**
     * 保存或修改
     *
     * @param userId     用户ID
     * @param roleIdList 角色ID列表
     */
    void saveRoleUser(Integer userId, List<Integer> roleIdList);

    /**
     * 根据角色ids，删除角色用户关系
     *
     * @param roleIds 角色ids
     */
    void deleteByRoleIds(Integer[] roleIds);

    /**
     * 根据用户id，删除角色用户关系
     *
     * @param userIds 用户ids
     */
    void deleteByUserIds(Integer[] userIds);

    /**
     * 角色ID列表
     *
     * @param userId 用户ID
     */
    List<Integer> getRoleIdList(Integer userId);
}