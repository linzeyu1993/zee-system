package com.zee.admin.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zee.admin.entity.SysParams;
import com.zee.admin.model.dto.SysParamsDTO;
import com.zee.admin.model.dto.SysParamsQueryDTO;
import com.zee.admin.model.excel.SysParamsExcel;

import java.util.HashMap;
import java.util.List;

/**
 * 参数管理
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public interface ISysParamsService extends IService<SysParams> {

    Page<SysParams> page(SysParamsQueryDTO dto);

    List<SysParamsExcel> exportList(HashMap<String, Object> params);

    void save(SysParamsDTO dto);

    void update(SysParamsDTO dto);
}
