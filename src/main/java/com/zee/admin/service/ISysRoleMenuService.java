package com.zee.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zee.admin.entity.SysRoleMenu;

import java.util.List;

/**
 * 系统角色与菜单对应关系
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public interface ISysRoleMenuService extends IService<SysRoleMenu> {

    /**
     * 根据角色ID，获取菜单ID列表
     */
    List<Integer> getMenuIdList(Integer roleId);

    /**
     * 保存或修改
     *
     * @param roleId     角色ID
     * @param menuIdList 菜单ID列表
     */
    void saveOrUpdate(Integer roleId, List<Integer> menuIdList);

    /**
     * 根据角色id，删除角色菜单关系
     *
     * @param roleIds 角色ids
     */
    void deleteByRoleIds(Integer[] roleIds);

    /**
     * 根据菜单id，删除角色菜单关系
     *
     * @param menuId 菜单id
     */
    void deleteByMenuId(Integer menuId);
}