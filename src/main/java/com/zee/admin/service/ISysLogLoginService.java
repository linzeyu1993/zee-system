package com.zee.admin.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zee.admin.entity.SysLogLogin;
import com.zee.admin.model.dto.SysLogLoginQueryDTO;

import java.util.HashMap;
import java.util.List;

/**
 * 登录日志
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public interface ISysLogLoginService extends IService<SysLogLogin> {

    Page<SysLogLogin> page(SysLogLoginQueryDTO dto);

    List<SysLogLogin> list(HashMap<String, Object> params);
}