package com.zee.admin.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zee.admin.entity.SysUser;
import com.zee.admin.model.dto.SysUserDTO;
import com.zee.admin.model.dto.SysUserQueryDTO;
import com.zee.admin.model.excel.SysUserExcel;
import com.zee.admin.model.vo.SysUserVO;

import java.util.HashMap;
import java.util.List;

/**
 * 系统用户
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public interface ISysUserService extends IService<SysUser> {

    SysUserVO getById(Integer id);

    Page<SysUserVO> page(SysUserQueryDTO sysUserQueryDTO);

    Integer count(HashMap<String, Object> params);

    List<SysUserExcel> exportList(HashMap<String, Object> params);

    SysUser getByUsername(String username);

    void save(SysUserDTO dto);

    void update(SysUserDTO dto);

    void delete(List<Integer> idList);

    /**
     * 修改密码
     *
     * @param id          用户ID
     * @param newPassword 新密码
     */
    void updatePassword(Integer id, String newPassword);

    /**
     * 根据部门ID，查询用户数
     */
    int getCountByDeptId(Integer deptId);

    /**
     * 根据部门ID,查询用户Id列表
     */
    List<Integer> getUserIdListByDeptId(List<Integer> deptIdList);

    Integer getCountBySuperAdmin(List<Integer> idList);

    List<SysUser> test(HashMap<String, Object> map);
}
