package com.zee.admin.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zee.admin.entity.SysLogOperation;
import com.zee.admin.model.dto.SysLogOperationQueryDTO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 操作日志
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public interface ISysLogOperationService extends IService<SysLogOperation> {

    Page<SysLogOperation> page(SysLogOperationQueryDTO dto);

    List<SysLogOperation> list(Map<String, Object> params);

    Integer count(HashMap<String, Object> params);
}