package com.zee.admin.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zee.admin.entity.SysRole;
import com.zee.admin.model.dto.SysRoleDTO;
import com.zee.admin.model.dto.SysRoleListDTO;
import com.zee.admin.model.dto.SysRoleQueryDTO;
import com.zee.admin.model.vo.SysRoleVO;

import java.util.List;

/**
 * 系统角色
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public interface ISysRoleService extends IService<SysRole> {

    Page<SysRole> page(SysRoleQueryDTO dto);

    List<SysRole> list(SysRoleListDTO dto);

    SysRoleVO info(Integer id);

    void save(SysRoleDTO dto);

    void update(SysRoleDTO dto);

    void delete(Integer[] ids);

}
