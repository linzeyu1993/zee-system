package com.zee.admin.interceptor;

/**
 * 数据范围
 * 此注解会使用DataFilterAspect切面，把sql注入到sqlFilter中
 * DataFilterInterceptor拦截器可以在dao层中，拦截hashMap键值中有dataScope，参数DataScope类型以及继承DataScopeDTO的类
 * 需要注意的是Dao层的接口参数，不管几个，都要用@Params注解
 *
 * @author Mark LinZee
 * @email LinZee666@163.com
 */
public class DataScope {
    private String sqlFilter;

    public DataScope(String sqlFilter) {
        this.sqlFilter = sqlFilter;
    }

    public String getSqlFilter() {
        return sqlFilter;
    }

    public void setSqlFilter(String sqlFilter) {
        this.sqlFilter = sqlFilter;
    }

    @Override
    public String toString() {
        return this.sqlFilter;
    }
}