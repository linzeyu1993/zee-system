### 欢迎关注
- 如果您觉得本系统好用，请帮忙Watch、Star项目，就是对本项目最好的支持

### 项目说明
- 开始是自己开发使用的，用的都是自己熟悉主流的技术，如今开源只为解决权限认证这一通用性强的功能。
- 帮助广大同仁快速开发单体应用，极低门槛，拿来即用，让一切都变得简单高效。
- 采用SpringBoot2、MyBatis-Plus、Shiro框架，开发的一套基于RBAC模型的权限认证系统。
- 演示地址：http://zee.bizhibk.com 帐号：admin/admin (自用服务器，请轻点测试，别删别人的数据)
- 说明文档：正在开发，请等待

### 项目结构
```
zee-system
├─common                公共模块
│   ├─config            配置文件
│   ├─cnstant           常量
│   ├─entity            实体基类
│   ├─enums             枚举
│   ├─exception         异常捕获、自定义异常等
│   ├─model             放置dto、vo、response等模型
│   ├─redis             redis配置文件、工具类
│   ├─utils             工具类
│   └─validator         校验分组
│ 
├─admin                 管理后台API接口
│   ├─annotation        注解
│   ├─aspect            切面
│   ├─config            配置文件
│   ├─controller        控制器
│   ├─dao               数据层
│   ├─entity            实体类
│   ├─enums             枚举
│   ├─interceptor       拦截器
│   ├─model             放置dto、vo、excel等模型
│   ├─service           服务层
│   └─shiro             放置shiro框架实体、过滤器、realm等
│       
│ 
├─home                  前台API接口
│   ├─annotation        注解
│   ├─config            配置文件
│   ├─controller        控制器
│   ├─dao               数据层
│   ├─entity            实体类
│   ├─interceptor       拦截器
│   ├─model             放置dto、vo、excel等模型
│   ├─resolver          解析器
│   └─service           服务层
│ 
├─resources 
│    ├─mapper           放置admin、home模块的MyBatis文件
│    ├─sql              mysql数据库sql
│    └─application.yml  全局配置文件
```


### 数据权限设计思想
- 用户管理、角色管理、部门管理，可操作本部门及子部门数据
- 菜单管理、参数管理、字典管理、系统日志，没有数据权限
- 业务功能，按照用户数据权限，查询、操作数据【没有本部门数据权限，也能查询本人数据】

**技术选型：**
- 核心框架：spring-boot 2.2.9.RELEASE
- 安全框架：Apache Shiro 1.10
- 持久层框架：MyBatis-Plus 3.4.3.4
- 数据库连接池：Druid 1.1.22
- 日志管理：Slf4j 1.2.17
- 接口文档：knife4j 2.0.8
  
**其它技术：**
- lombok 1.16.22
- fastjson 1.2.69
- hutool 5.7.1
- guava 20.0
- easy-captcha 1.6.2
- easyexcel 3.1.0

<br>

**软件需求**
- JDK1.8
- Maven3.0+
- MySQL5.7+
